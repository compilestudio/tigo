<?php

	/*
	 * Fecha de Creación: 22-dic-2011
	 * Autor: Victor Lopez
	 * Fecha Última Modificación: 
	 * Modificado por: 
	 * Descripción: 
	 * 
	 */

class File_model extends MY_Model {

    public function processExcel($path,$file_,$cont=0){
        $file = $path.$file_;
        $this->load->library('xlsxreader');

        if(is_file($file)):
            //load our new PHPExcel library
            $array = array($file);
            $this->xlsxreader->init($array);

            $sheets = $this->xlsxreader->getSheetNames();
            $sheet = reset($sheets);

            $data = $this->xlsxreader->getSheetData($sheet);

            #Escribimos el log
            echo $file_.": ".count($data)." records<br>";
            log_message('info', $file_.": ".count($data)." records");

            #$array = array();
            $array = array( "No_Hps" => 0 ,"NODO_VERSION" => 0 ,"HUB" => 0 ,"NODO" => 0 ,"POSTE" => 0 ,"BOULEVARD" => 0 ,"AVENIDA" => 0 ,"CALLE" => 0 ,"CALLEJON" => 0 ,"CALZADA" => 0 ,"CARRETERA" => 0 ,"KILOMETRO" => 0 ,"DIAGONAL" => 0 ,"VIA" => 0 ,"RUTA" => 0 ,"CASA_No" => 0 ,"CONTADOR" => 0 ,"ZONA" => 0 ,"EDIFICIO" => 0 ,"NIVEL" => 0 ,"APARTAMENTO" => 0 ,"COLONIA" => 0 ,"CONDOMINIO" => 0 ,"RESIDENCIAL" => 0 ,"URBANIZACION" => 0 ,"COMERCIO" => 0 ,"C_COMERCIAL" => 0 ,"INSTITUCION" => 0 ,"No_LOCAL" => 0 ,"OFICINA" => 0 ,"PLAZA" => 0 ,"LOTE" => 0 ,"CLUSTER" => 0 ,"CONDADO" => 0 ,"MANZANA" => 0 ,"PROYECTO" => 0 ,"SECTOR" => 0 ,"FASE" => 0 ,"TRONCO" => 0 ,"FINCA" => 0 ,"ALDEA" => 0 ,"CASERIO" => 0 ,"CANTON" => 0 ,"CIUDAD" => 0 ,"MUNICIPIO" => 0 ,"DEPARTAMENTO" => 0 ,"OPERADOR" => 0 ,"RED" => 0 ,"LOCALIZACION" => 0 ,"CATASTRAL" => 0 ,"GRANJA" => 0 ,"ANEXO" => 0 ,"FRACCION" => 0 ,"INTERIOR" => 0 ,"MODULO" => 0 ,"CERRO" => 0 ,"TORRE" => 0 ,"KIOSCO" => 0 ,"PARCELA" => 0 ,"SALON" => 0 ,"RESTAURANTE" => 0 ,"REFERENCIA" => 0 ,"BARRIO" => 0 ,"PASAJE" => 0 ,"MERCADO" => 0 ,"ASENTAMIENTO" => 0 ,"EJE" => 0 ,"LOTIFICACION" => 0 ,"SECCION" => 0 ,"DIRECCION" => 0, "OBSERVACIONES" => 0 ,"LAT" => 0 ,"LON" => 0 );

            echo count($data[3])."<br>";
            #if(count($data[3])==73):
                for($i=4;$i<count($data);$i++):
                    $j=0;
                    foreach($array as $x => $value):
                        $array[$x] = $data[$i][$j++];
                    endforeach;

/*
                    $this->load->library('gpoint');
                    if(!empty($array['LAT'])):
                        $this->gpoint->setUTM($array['LAT'],$array['LON'],"15N");
                        $this->gpoint->convertTMtoLL();
                        $array['LAT'] = $this->gpoint->Lat();
                        $array['LON'] = $this->gpoint->Long();
                    endif;
                    */

                    #Vemos si existe no lo insertamos
                    #$query_existe = $this->db->get_where('direcciones', $array);
                    #if ($query_existe->num_rows() <= 0):
                        $this->db->insert('direcciones', $array);
                    #endif;
                endfor;

                #We mov the files to the new folder
                #rename($path.$file_, $path."procesados/".$file_);
            #else:
            #    rename($path.$file_, $path."no/".$file_);
            #endif;

            $this->xlsxreader->close();

        endif; #End if is file
    }
#********************************************************************************
    public function processCoordenadas($path,$file_,$cont=0){
        $file = $path.$file_;
        $this->load->library('xlsxreader');

        $nodo = explode(" ",$file_);
        $nodo = $nodo[0];

        if(is_file($file)):
            //load our new PHPExcel library
            $array = array($file);
            $this->xlsxreader->init($array);

            $data = $this->xlsxreader->getSheetData(1);

            #Escribimos el log
            echo $file_.": ".count($data)." records<br>";
            log_message('info', $file_.": ".count($data)." records");

            if(count($data[0])==3):
                for($i=1;$i<count($data);$i++):

                    $array = array(
                        'casa_lote' => $data[$i][0],
                        'lat' => $data[$i][1],
                        'lon' => $data[$i][2],
                        'nodo' => $nodo
                    );

                    $this->db->insert('coordenadas', $array);

                    /*
                    if($cont>0):
                        var_dump($data[$i]);
                        echo "<br><br>";
                        echo "<pre>";
                        var_dump($array);
                        echo "</pre><br><br>";
                    endif;
                    */
                endfor;

                #We mov the files to the new folder
                rename($path.$file_, $path."procesados/".$file_);
            else:
                rename($path.$file_, $path."no/".$file_);
            endif;

            $this->xlsxreader->close();
        endif; #End if is file
    }
}	