<?php

	/*
	 * Fecha de Creación: 22-dic-2011
	 * Autor: Victor Lopez
	 * Fecha Última Modificación: 
	 * Modificado por: 
	 * Descripción: 
	 * 
	 */

class Session_model extends MY_Model {
	
	function __construct() {
		
		parent::__construct();
		$this->set_table('user');
		
	}
	
	public function authorized($username, $password) {
			
		$authorized = false;

		#destruimos la sesión actual
		$this->session->unset_userdata('user_tigo');
		
		#buscamos un registro que coincida con el usuaruio
		$this->db->select('id, password,nombre,tipo,force');
		$this->db->from('user');
		$this->db->where('email', $username);
		
		$query = $this->db->get();
		
		#si encontramos un usuario
		if ($query->num_rows() == 1):
			
			$user = $query->row();
			
			#desencriptamos la contraseña de la base de datos
			$this->load->library('encrypt');
			$plain_password = $this->encrypt->decode($user->password);	
			$authorized = ($password === $plain_password);

			if($authorized):
				
				#cargamos los datos del usuario a la sesión
				$row = $query->row();
				$this->session->set_userdata('user_tigo', $user->id);
				$this->session->set_userdata('user_name', $user->nombre);
				$this->session->set_userdata('tigo_tipo', $user->tipo);
                $this->session->set_userdata('tigo_force', $user->force);
				$this->session->unset_userdata('new_expiration');

				$this->save_login($user->id);
			endif;
		endif;
			
		return ($authorized);
		
	}

	public function save_login($user){
		#Guardamos el login en la bitacora
		$this->set_table("bitacora");
		$array = array (
			'user' => $user,
			'login' => date('Y-m-d G:i:s'),
			'logout' => date('Y-m-d G:i:s')
		);
		$this->insert($array);
	}
	
	public function remember() {
		$this->session->set_userdata('new_expiration', 2419200); //4 weeks
	}
	
}	