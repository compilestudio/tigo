<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends MY_Controller {

#**************************************************************************************************
	public function index($error=0){

		$this->load->helper('path');

        /*
        $x = "763911.59";
		$y = "1615246.98";

		$this->load->library('gpoint');
		$this->gpoint->setUTM($x,$y,"15N");
		$this->gpoint->convertTMtoLL();
		$lat = $this->gpoint->Lat();
		$lon = $this->gpoint->Long();
		echo $lat.",".$lon."<br>";

        $x = "763996.14";
        $y = "1615187.91";
        $this->gpoint->setUTM($x,$y,"15N");
        $this->gpoint->convertTMtoLL();
        $lat = $this->gpoint->Lat();
        $lon = $this->gpoint->Long();
        echo $lat.",".$lon;
*/

		if($this->session->userdata('logged_user')){
			redirect('landing/principal');
		}else{
			$this->load_header(NULL);
			$this->load->view('landing');	
			$this->load_footer();
		}
	}
#**************************************************************************************************	
	public function principal(){
		$this->logged();
		$data['dat'] = $this->session->all_userdata();
		$this->load_header(NULL);
		$this->load->view('principal', $data);
		$this->load_footer();
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */