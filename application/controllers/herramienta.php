<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Herramienta extends MY_Controller {
#**************************************************************************************************
    public function listado_colonias($op=0){
        $this->load_header(NULL);

        #Colonias Adquiridas
        $sql = "SELECT  nombre colonias
                FROM    `poligono`
                WHERE   tipo =1";
        $query = $this->db->query($sql);
        $adquiridas = null;
        if ($query->num_rows() > 0):
            $adquiridas = $query->result();
        endif;

        #Colonias Nuevas
        $sql = "SELECT distinct TRIM(IFNULL(colonia,IFNULL(condominio,IFNULL(residencial,IFNULL(urbanizacion,IFNULL(barrio,NULL)))))) colonias
                FROM `direcciones`
                having LENGTH(colonias)>5
                order by colonias asc ";
        $query = $this->db->query($sql);
        $nuevas = NULL;
        if ($query->num_rows() > 0):
            $nuevas = $query->result();
        endif;

        $data['op'] = $op;
        if($op==0):
            $data['rows'] = $nuevas;
        else:
            $data['rows'] = $adquiridas;
        endif;


        $this->load->view('listado_colonias',$data);
        $this->load_footer();
    }
}