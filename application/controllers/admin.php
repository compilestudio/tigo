<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Admin extends MY_Controller {
 
    function __construct()
    {
        parent::__construct();
 
        /* Standard Libraries of codeigniter are required */
        $this->load->database();
        $this->load->helper('url');
        /* ------------------ */ 
 
        $this->load->library('grocery_CRUD');
 
    }
 
    public function usuario(){
        $this->load_header(NULL);

        $sql = "SELECT  u.*,tu.tipo as tipo_ 
                FROM    user u,tipo_usuario tu
                WHERE   u.tipo=tu.id
                order   by u.nombre asc";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0):
            $data['usuarios'] = $query->result();
        else:
            $data['usuarios'] = NULL;
        endif;
        
        $this->load->model("file_model","model");
        $this->model->set_table("tipo_usuario");
        $data['tipos'] = $this->model->list_rows("*");
        $this->model->set_table("departamento");
        $data['deptos'] = $this->model->list_rows("*");

        $this->load->view('listado_usuarios',$data);
        $this->load_footer();
    }
    public function eliminar_usuario(){
        $id = $this->input->post("id");
        $this->load->model("file_model","model");
        $this->model->set_table("user");

        $this->model->delete($id);
    }
    public function guardar_edits(){
        $this->load->library('encrypt');
        $this->load->model("file_model","model");
        $this->model->set_table("user");

        $id = $this->input->post("id");
        $usuario = $this->input->post("usuario");
        $nombre = $this->input->post("nombre");
        $password = $this->input->post("password");
        $tipo = $this->input->post("tipo");
        $departamento = $this->input->post("departamento");

        $where = "";
        if(!empty($id)):
            $where = " and id != ".$id;
        endif;
        
        $existe = $this->model->list_rows("id","email='" . $usuario . "'".$where);

        if(!$existe):

            $array = array(
                            'email' => $usuario,
                            'tipo' => $tipo,
                            'nombre' => $nombre,
                            'departamento' => $departamento
                        );

            #Si viene password hay que cambiarlo y forzar a que lo cambien en su proxima logeada
            if(!empty($password)):
                $array['password'] = $this->encrypt->encode($password);
                $array['force'] = 1;
            endif;

            if(!empty($id)):
                $this->model->update($array,$id);
            else:
                $this->model->insert($array);
            endif;
            echo 1;
        else:
            echo 0;
        endif;
    }
#******************************************************************
    function validate_user(){
        $usuario = $this->input->get("fieldValue");
        $id = $this->input->get("fieldId");
        $this->load->model("file_model","model");
        $this->model->set_table("user");
        $existe = $this->model->list_rows("id","email='" . $usuario . "'");
        if(!$existe){
            echo  json_encode(array($id,1));
        }else{
            echo  json_encode(array($id,0));
        }

    }
#******************************************************************
##******************************************************************
##******************************************************************
##******************************************************************
    public function usuarios()
    {     

        $crud = new grocery_CRUD();
        //Seleccionamos un theme de Grocey Crud ver la carpeta theme dentro de assets se encuentra todo lo necesario en dado caso no sea espesificado el thema se usara el de default
        //$crud->set_theme('twitter-bootstrap');
        $crud->set_theme('JCrud');
        //Seleccionamos el lenguaje deseado
        $crud->set_language("spanish");        

        //Selecionamos la Tabla a desplegar sus valores 
        $crud->set_table("user");
        $crud->change_field_type('password','password');
        $crud->display_as('email','Usuario');

        $crud->callback_before_insert(array($this,'encrypt_password_callback'));
        $crud->callback_before_update(array($this,'encrypt_password_callback'));
        $crud->callback_edit_field('password',array($this,'decrypt_password_callback'));
        $crud->unset_columns('force');
        #$crud->unset_edit_fields('force');
        $crud->unset_add_fields('force');

        $crud->required_fields('email','password','tipo','nombre');

        $crud->set_rules('password','Password Check','callback_pass_check');


        $crud->set_subject('Usuario');

        $crud->set_relation('tipo','tipo_usuario','tipo');
        //Renderizamos el CRUD
        $output = $crud->render();
        $this->load_header(NULL);
        $this->load->view('admin/index.php',$output);  
        $this->load_footer();       
    }
#******************************************************************
    function encrypt_password_callback($post_array, $primary_key = null){
        $this->load->library('encrypt');
        $post_array['password'] = $this->encrypt->encode($post_array['password']);
        return $post_array;
    }
#******************************************************************
    function decrypt_password_callback($value){
        $this->load->library('encrypt');
        $decrypted_password = $this->encrypt->decode($value);
        return "<input type='password' name='password' value='$decrypted_password' />
                <input type='hidden' name='previous' value='$decrypted_password' />";
    }
#******************************************************************
#******************************************************************
    public function pass_check($pwd){

        $error = "";

        if( strlen($pwd) > 20 ) {
            $error .= "Su contraseña es muy larga!<br>
";
        }

        if( strlen($pwd) < 8 ) {
            $error .= "Su contraseña es muy corta, debe de tener por lo menos 8 caracteres!<br>";
        }

        if( !preg_match("#[0-9]+#", $pwd) ) {
            $error .= "Su Su contraseña debe de contener por lo menos un número!<br>";
        }


        if( !preg_match("#[a-z]+#", $pwd) ) {
            $error .= "Su contraseña debe de tener por lo menos una letra!<br>";
        }


        if( !preg_match("#[A-Z]+#", $pwd) ) {
            $error .= "Su contraseña debe de tener por lo menos una mayúscula!<br>";
        }



        if( !preg_match("#\W+#", $pwd) ) {
            $error .= "Su contraseña debe de tener por lo menos un símbolo!<br>";
        }


        if($error){
            $this->form_validation->set_message('pass_check', $error);
            return false;
        } else {
            return true;
        }

    }

#******************************************************************
#******************************************************************
#******************************************************************
    public function ips()
    {

        $crud = new grocery_CRUD();
        //Seleccionamos un theme de Grocey Crud ver la carpeta theme dentro de assets se encuentra todo lo necesario en dado caso no sea espesificado el thema se usara el de default
        //$crud->set_theme('twitter-bootstrap');
        $crud->set_theme('JCrud');
        //Seleccionamos el lenguaje deseado
        $crud->set_language("spanish");

        //Selecionamos la Tabla a desplegar sus valores
        $crud->set_table("tr_cldr");
        $crud->display_as('mjm','Ip Autorizada');

        #$crud->callback_before_insert(array($this,'encrypt_ip_callback'));
        #$crud->callback_before_update(array($this,'encrypt_ip_callback'));
        #$crud->callback_edit_field('mjm',array($this,'decrypt_ip_callback'));

        $crud->required_fields('mjm');

        $crud->set_subject('Ips autorizadas');

        //Renderizamos el CRUD
        $output = $crud->render();
        $this->load_header(NULL);
        $this->load->view('admin/index.php',$output);
        $this->load_footer();
    }
#******************************************************************
    function encrypt_ip_callback($post_array, $primary_key = null){
        $this->load->library('encrypt');
        $post_array['mjm'] = $this->encrypt->encode($post_array['mjm']);
        return $post_array;
    }
#******************************************************************
    function decrypt_ip_callback($value){
        $this->load->library('encrypt');
        $decrypted_password = $this->encrypt->decode($value);
        return "<input type='mjm' name='mjm' value='$decrypted_password' />";
    }
}

 