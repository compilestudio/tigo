<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reporte extends MY_Controller {
#**************************************************************************************************
    public function coordenadas_cero($guardo=0,$nodo=""){
        if($nodo=="")
            $nodo = $this->input->post("nodo");
        $data['nodo_seleccionado'] =$nodo;
        $data['guardo'] =$guardo;
        $this->load_header(NULL);

        $sql = "select  *
                from    direcciones
                WHERE   (lat =0
                OR      lon =0) ";
        if(!empty($nodo)):
            $sql .= " and nodo like '".$nodo."' ";
        endif;
        $sql .= " order by manzana asc,lote asc,avenida asc, calle asc,casa_no asc limit 0,25";
        $query = $this->db->query($sql);
        $data['rows'] = $query->result();

        #Obtenemos el listado de los nodos
        $sql = "SELECT distinct nodo,count(1) cant
                FROM `direcciones`
                where lat =0
                or lon =0
                group by nodo";
        $query = $this->db->query($sql);
        $data['nodos'] = $query->result();

        $this->load->view('coordenadas_cero',$data);
        $this->load_footer();
    }
#**************************************************************************************************
    public function save(){
        $this->load->library('gpoint');
        $ids = $this->input->post("id");
        $lat = $this->input->post("latitud");
        $lon = $this->input->post("longitud");
        $nodo = $this->input->post("nodo");

        $guardo =0;

        for($i=0;$i<count($ids);$i++):
            if(trim($lat[$i])!='' && trim($lon[$i])!=''):
                $this->gpoint->setUTM($lat[$i],$lon[$i],"15N");
                $this->gpoint->convertTMtoLL();

                $data = array(
                    'lat' => $this->gpoint->Lat(),
                    'lon' => $this->gpoint->Long()
                );
                $this->db->where('id',$ids[$i]);
                $this->db->update('direcciones', $data);
                $guardo =1;
            endif;
        endfor;
        redirect("reporte/".$guardo."/".$nodo);

    }
#**************************************************************************************************
    public function descarga_nodo($nodo=""){
        if($nodo=="")
            $nodo = $this->input->post("nodo");
        $data['nodo_seleccionado'] =$nodo;
        #Obtenemos el listado de los nodos
        $sql = "SELECT distinct nodo,count(1) cant
                FROM `direcciones`
                group by nodo";
        $query = $this->db->query($sql);
        $data['nodos'] = $query->result();


        $sql = "select  *
                from    direcciones";
        if(!empty($nodo)):
            $sql .= " where nodo like '".$nodo."' ";
        endif;
        $sql .= " limit 0,25";
        $query = $this->db->query($sql);
        $data['rows'] = $query->result();

        $this->load_header(NULL);
        $this->load->view('descarga_nodo',$data);
        $this->load_footer();
    }
#**************************************************************************************************
    public function descarga_excel($nodo=""){
        $this->load->library('excel');

        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('DIRECCIONES');

        #PONEMOS EL TITULO COORDENADAS
        $this->excel->getActiveSheet()->SetCellValue('BU3', "COORDENADAS");
        $this->excel->getActiveSheet()->mergeCells('BU3:BU3');
        $this->excel->getActiveSheet()->getStyle('BU3:BU3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        #Le ponemos el borde a las coordenadas
        $styleArray = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );
        $this->excel->getActiveSheet()->getStyle('BU3:BU3')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('BU3:BU3')->getFont()->setBold(true);


        $this->excel->getActiveSheet()->getStyle('A4:D4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF2D5884');

        #Centramos todas las columnas
        $this->excel->getActiveSheet()->getStyle('A4:BU4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        #Le ponemos el borde a las columnas
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );
        $this->excel->getActiveSheet()->getStyle('A4:BU4')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('A4:BU4')->getFont()->setBold(true);

        #NOMBRE DE LAS COLUMNAS
        $this->excel->getActiveSheet()->SetCellValue('A4', "No. Hp's");
        $this->excel->getActiveSheet()->SetCellValue('B4', "NODO # VERSION");
        $this->excel->getActiveSheet()->SetCellValue('C4', "HUB");
        $this->excel->getActiveSheet()->SetCellValue('D4', "NODO");
        $this->excel->getActiveSheet()->SetCellValue('E4', "POSTE");
        $this->excel->getActiveSheet()->SetCellValue('F4', "BOULEVARD");
        $this->excel->getActiveSheet()->SetCellValue('G4', "AVENIDA");
        $this->excel->getActiveSheet()->SetCellValue('H4', "CALLE");
        $this->excel->getActiveSheet()->SetCellValue('I4', "CALLEJON");
        $this->excel->getActiveSheet()->SetCellValue('J4', "CALZADA");
        $this->excel->getActiveSheet()->SetCellValue('K4', "CARRETERA");
        $this->excel->getActiveSheet()->SetCellValue('L4', "KILOMETRO");
        $this->excel->getActiveSheet()->SetCellValue('M4', "DIAGONAL");
        $this->excel->getActiveSheet()->SetCellValue('N4', "VIA");
        $this->excel->getActiveSheet()->SetCellValue('O4', "RUTA");
        $this->excel->getActiveSheet()->SetCellValue('P4', "CASA No.");
        $this->excel->getActiveSheet()->SetCellValue('Q4', "CONTADOR");
        $this->excel->getActiveSheet()->SetCellValue('R4', "ZONA");
        $this->excel->getActiveSheet()->SetCellValue('S4', "EDIFICIO");
        $this->excel->getActiveSheet()->SetCellValue('T4', "NIVEL");
        $this->excel->getActiveSheet()->SetCellValue('U4', "APARTAMENTO");
        $this->excel->getActiveSheet()->SetCellValue('V4', "COLONIA");
        $this->excel->getActiveSheet()->SetCellValue('W4', "CONDOMINIO");
        $this->excel->getActiveSheet()->SetCellValue('X4', "RESIDENCIAL");
        $this->excel->getActiveSheet()->SetCellValue('Y4', "URBANIZACION");
        $this->excel->getActiveSheet()->SetCellValue('Z4', "COMERCIO");
        $this->excel->getActiveSheet()->SetCellValue('AA4', "C. COMERCIAL");
        $this->excel->getActiveSheet()->SetCellValue('AB4', "INSTITUCION");
        $this->excel->getActiveSheet()->SetCellValue('AC4', "No. LOCAL");
        $this->excel->getActiveSheet()->SetCellValue('AD4', "OFICINA");
        $this->excel->getActiveSheet()->SetCellValue('AE4', "PLAZA");
        $this->excel->getActiveSheet()->SetCellValue('AF4', "LOTE");
        $this->excel->getActiveSheet()->SetCellValue('AG4', "CLUSTER");
        $this->excel->getActiveSheet()->SetCellValue('AH4', "CONDADO");
        $this->excel->getActiveSheet()->SetCellValue('AI4', "MANZANA");
        $this->excel->getActiveSheet()->SetCellValue('AJ4', "PROYECTO");
        $this->excel->getActiveSheet()->SetCellValue('AK4', "SECTOR");
        $this->excel->getActiveSheet()->SetCellValue('AL4', "FASE");
        $this->excel->getActiveSheet()->SetCellValue('AM4', "TRONCO");
        $this->excel->getActiveSheet()->SetCellValue('AN4', "FINCA");
        $this->excel->getActiveSheet()->SetCellValue('AO4', "ALDEA");
        $this->excel->getActiveSheet()->SetCellValue('AP4', "CASERIO");
        $this->excel->getActiveSheet()->SetCellValue('AQ4', "CANTON");
        $this->excel->getActiveSheet()->SetCellValue('AR4', "CIUDAD");
        $this->excel->getActiveSheet()->SetCellValue('AS4', "MUNICIPIO");
        $this->excel->getActiveSheet()->SetCellValue('AT4', "DEPARTAMENTO");
        $this->excel->getActiveSheet()->SetCellValue('AU4', "OPERADOR");
        $this->excel->getActiveSheet()->SetCellValue('AV4', "RED");
        $this->excel->getActiveSheet()->SetCellValue('AW4', "LOCALIZACION");
        $this->excel->getActiveSheet()->SetCellValue('AX4', "CATASTRAL");
        $this->excel->getActiveSheet()->SetCellValue('AY4', "GRANJA");
        $this->excel->getActiveSheet()->SetCellValue('AZ4', "ANEXO");
        $this->excel->getActiveSheet()->SetCellValue('BA4', "FRACCION");
        $this->excel->getActiveSheet()->SetCellValue('BB4', "INTERIOR");
        $this->excel->getActiveSheet()->SetCellValue('BC4', "MODULO");
        $this->excel->getActiveSheet()->SetCellValue('BD4', "CERRO");
        $this->excel->getActiveSheet()->SetCellValue('BE4', "TORRE");
        $this->excel->getActiveSheet()->SetCellValue('BF4', "KIOSCO");
        $this->excel->getActiveSheet()->SetCellValue('BG4', "PARCELA");
        $this->excel->getActiveSheet()->SetCellValue('BH4', "SALON");
        $this->excel->getActiveSheet()->SetCellValue('BI4', "RESTAURANTE");
        $this->excel->getActiveSheet()->SetCellValue('BJ4', "REFERENCIA");
        $this->excel->getActiveSheet()->SetCellValue('BK4', "BARRIO");
        $this->excel->getActiveSheet()->SetCellValue('BL4', "PASAJE");
        $this->excel->getActiveSheet()->SetCellValue('BM4', "MERCADO");
        $this->excel->getActiveSheet()->SetCellValue('BN4', "ASENTAMIENTO");
        $this->excel->getActiveSheet()->SetCellValue('BO4', "EJE");
        $this->excel->getActiveSheet()->SetCellValue('BP4', "LOTIFICACION");
        $this->excel->getActiveSheet()->SetCellValue('BQ4', "SECCION");
        $this->excel->getActiveSheet()->SetCellValue('BR4', "DIRECCION COMPLETA");
        #$this->excel->getActiveSheet()->SetCellValue('BS4', "ID DIRECCION");
        $this->excel->getActiveSheet()->SetCellValue('BS4', "OBSERVACIONES");
        $this->excel->getActiveSheet()->SetCellValue('BT4', "LAT");
        $this->excel->getActiveSheet()->SetCellValue('BU4', "LONG");

        $sql = "select  *
                from    direcciones
                where nodo like '".$nodo."'";
        $query = $this->db->query($sql);
        $array_info = $query->result_array();

        foreach ($array_info as &$element) {
            $element = array_slice($element, 1);
        }


        $this->excel->getActiveSheet()->fromArray($array_info, NULL, 'A5');

        #Le ponemos el borde a las columnas
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );
        $this->excel->getActiveSheet()->getStyle('A5:BU'.(count($array_info)+4))->applyFromArray($styleArray);

        for($col = 'A'; $col !== 'BV'; $col++) {
            $this->excel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        $filename=$nodo.'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');

/*
        $info = $this->load->view('nodo_excel',$data,true);

        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=exceldata.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $info;
*/
    }
#**************************************************************************************************
    public function nodos_cargados(){

        $depto = $this->input->post("departamento");
        $muni = $this->input->post("municipio");
        $zona = $this->input->post("zona");
        $descargar = $this->input->post("descargar");
        $data["depto"] = $depto;
        $data["muni"] = $muni;
        if(empty($descargar)):
            $data["descargar"] = 0;
        else:
            $data["descargar"] = $descargar;
        endif;
        if($zona)
            $data["zona"] = $zona;
        else
            $data["zona"] = NULL;
        if(!empty($zona))
            $zona = implode("','",$zona);

        $sql = "SELECT distinct departamento
                FROM catalogo
                order by departamento asc";
        $query = $this->db->query($sql);
        $data['deptos'] = $query->result();

        if($depto!=""):
            $sql = "SELECT distinct municipio
                FROM    catalogo
                where   departamento= '".$depto."'
                order by municipio asc";
            $query = $this->db->query($sql);
            $data['munis'] = $query->result();

            if($muni!=""):
                $sql = "SELECT distinct zona
                FROM    catalogo
                where   departamento= '".$depto."'
                and     municipio= '".$muni."'
                order by convert(zona, decimal) asc";
                $query = $this->db->query($sql);
                $data['zonas'] = $query->result();
            else:
                $data['zonas'] = NULL;
            endif;
        else:
            $data['munis'] = NULL;
        endif;


        if(empty($depto)):
            #$sql = "SELECT  DEPARTAMENTO,MUNICIPIO,ZONA,HUB,nodo, count(1) cant,sum(IF(lat is not null or lat != '', 1, 0)) coords,sum(IF(lat is null or lat = '', 1, 0)) sin_coords
            $sql = "SELECT  DEPARTAMENTO,MUNICIPIO,ZONA,hub,nodo, count(1) cant
                    FROM    `direcciones`
                    group   by nodo
                    order   by hub asc,nodo asc ";
        elseif(empty($zona)):
            $sql = "SELECT  DEPARTAMENTO,MUNICIPIO,ZONA,hub,nodo, count(1) cant
                    FROM    direcciones
                    where nodo in (select nodo from catalogo where departamento like '%".$depto."%' and municipio like '%".$muni."%')
                    group   by nodo
                    order   by hub asc,nodo asc";
        else:
            $sql = "SELECT  DEPARTAMENTO,MUNICIPIO,ZONA,hub,nodo, count(1) cant
                    FROM    direcciones
                    where nodo in (select nodo from catalogo where departamento like '%".$depto."%' and municipio like '%".$muni."%' and zona in ('".$zona."'))
                    group   by nodo
                    order   by hub asc,nodo asc";
        endif;

        $query = $this->db->query($sql);
        $data['rows'] = $array_info = $query->result_array();

        $sql = "SELECT  count(1) cant
                FROM    `direcciones`";
        $query = $this->db->query($sql);
        $data['direcciones'] = $query->result();

        if($descargar==0):
            $this->load_header(NULL);
            $this->load->view('nodos_cargados',$data);
            $this->load_footer();
        else:
            #Descargamos el archivo en XLS
            $this->load->library('excel');

            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('Nodos');

            #Centramos todas las columnas
            $this->excel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            #NOMBRE DE LAS COLUMNAS
            $this->excel->getActiveSheet()->SetCellValue('A1', "Departamento");
            $this->excel->getActiveSheet()->SetCellValue('B1', "Municipio");
            $this->excel->getActiveSheet()->SetCellValue('C1', "Zona");
            $this->excel->getActiveSheet()->SetCellValue('D1', "HUB");
            $this->excel->getActiveSheet()->SetCellValue('E1', "Nodo");
            $this->excel->getActiveSheet()->SetCellValue('F1', "Cantidad de direcciones");

            $this->excel->getActiveSheet()->fromArray($array_info, NULL, 'A2');

            #Auto resize
            for($col = 'A'; $col !== 'G'; $col++) {
                $this->excel->getActiveSheet()
                    ->getColumnDimension($col)
                    ->setAutoSize(true);
            }

            $filename='nodos_cagados.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            $objWriter->save('php://output');
        endif;
    }
#**************************************************************************************************
    public function registro_clientes(){
        $this->load_header(NULL);

        $sql = "select  *
                from    registro_cliente";
        $query = $this->db->query($sql);
        $data['rows'] = $query->result();

        $this->load->view('registro_clientes',$data);
        $this->load_footer();
    }
#**************************************************************************************************
    public function guardar_edits(){
        $this->load->library('encrypt');
        $this->load->model("file_model","model");
        $this->model->set_table("registro_cliente");

        $id = $this->input->post("id");
        $nombre = $this->input->post("nombre");
        $direccion = $this->input->post("direccion");
        $email = $this->input->post("email");
        $telefono = $this->input->post("telefono");
        $longitud = $this->input->post("longitud");
        $latitud = $this->input->post("latitud");
        $comentarios = $this->input->post("comentarios");

        $array = array(
            'nombre' => $nombre,
            'direccion' => $direccion,
            'email' => $email,
            'telefono' => $telefono,
            'longitud' => $longitud,
            'latitud' => $latitud,
            'comentarios' => $comentarios,
        );

        $this->model->update($array,$id);

        echo 1;
    }
#**************************************************************************************************
    public function eliminar_registro(){
        $id = $this->input->post("id");
        $this->load->model("file_model","model");
        $this->model->set_table("registro_cliente");

        $this->model->delete($id);
    }
#**************************************************************************************************
    public function descargar_clientes(){
        $this->load->library('excel');

        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Clientes');

        #Centramos todas las columnas
        $this->excel->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        #NOMBRE DE LAS COLUMNAS
        $this->excel->getActiveSheet()->SetCellValue('A1', "Nombre");
        $this->excel->getActiveSheet()->SetCellValue('B1', "Dirección");
        $this->excel->getActiveSheet()->SetCellValue('C1', "Email");
        $this->excel->getActiveSheet()->SetCellValue('D1', "Teléfono");
        $this->excel->getActiveSheet()->SetCellValue('E1', "Longitud");
        $this->excel->getActiveSheet()->SetCellValue('F1', "Latitud");
        $this->excel->getActiveSheet()->SetCellValue('G1', "Comentarios");

        $sql = "select  nombre,direccion,email,telefono,longitud,latitud,comentarios
                from    registro_cliente
                WHERE   user=".$this->session->userdata('user_tigo');
        $query = $this->db->query($sql);
        $array_info = $query->result_array();

        $this->excel->getActiveSheet()->fromArray($array_info, NULL, 'A2');

        #Auto resize
        for($col = 'A'; $col !== 'G'; $col++) {
            $this->excel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        $filename='clientes.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
    }
#**************************************************************************************************
	/**
     * @param int $page
     */
    public function control_usuarios($page=0){
        #Obtenemos las variables
        $ini = $this->input->get("start");
        $fin = $this->input->get("end");
        $data['ini'] = $ini;
        $data['fin'] = $fin;
        $data['nombre'] = $nombre = $this->input->get("nombre");
        $data['usuario'] = $usuario = $this->input->get("usuario");
        $data['departamento'] = $departamento = $this->input->get("departamento");
        $data['unidad'] = $unidad = $this->input->get("unidad");
        $data['perfil'] = $perfil = $this->input->get("perfil");
        $data['order_by'] = $order_by = $this->input->get("order_by");
        $data['direccion'] = $direccion = $this->input->get("direccion");
        $data['activo'] = $activo = $this->input->get("activo");
        $tipo_reporte = $this->input->get("tipo_reporte");
        if(!empty($tipo_reporte)):
            $data['tipo_reporte'] = $tipo_reporte;
        else:
            $data['tipo_reporte'] = $tipo_reporte = 1;
        endif;
        $descargar = $this->input->get("descargar");
        $where = " and    u.nombre like '%".$nombre."%'
                   and      u.email like '%".$usuario."%'";
        if(!empty($departamento) && $departamento!=-1):
            $where .= " and u.departamento like '%".$departamento."%' ";
        endif;
        if(!empty($unidad) && $unidad!=-1):
            $where .= " and un.id=".$unidad." ";
        endif;
        if(!empty($perfil) && $perfil!=-1):
            $where .= " and tu.id=".$perfil." ";
        endif;

        #Bandera de descargar archivo
        if(empty($descargar)):
            $data["descargar"] = 0;
        else:
            $data["descargar"] = $descargar;
        endif;

        if($activo==3):
            #Inactivo
            $where .= " and total_minutos is null";
        elseif($activo==2):
            #Activo
            $where .= " and total_minutos is not null";
        else:
            #Todos
        endif;

        #Tipo de reporte
        if($tipo_reporte==2):
            #$campos = "u.nombre,u.email,d.name departamento,un.unidad,u.creado,tu.tipo tipo_usuario,bd.date fecha_bitacora,total_minutos,visitas,promedio_minutos";
            $campos = "u.nombre,u.email,sum(total_minutos) total_minutos,sum(visitas) visitas, round(avg(promedio_minutos)) promedio_minutos";
            $group = " group by u.email ";
        else:
            $campos = "bd.date fecha_bitacora,u.nombre,u.email,total_minutos,visitas,promedio_minutos,horas";
            $group = "";
        endif;

        if (!empty($ini) && !empty($fin)):
            $ini = explode("/", $ini);
            $fin = explode("/", $fin);

            $ini = $ini[2] . "-" . $ini[1] . "-" . $ini[0];
            $fin = $fin[2] . "-" . $fin[1] . "-" . $fin[0];
            #$where .= " and u.creado between '".$ini." 00:00:00' and '".$fin." 23:59:59' ";
            $where .= " and bd.date between '".$ini." 00:00:00' and '".$fin." 23:59:59' ";
        endif;

        /*
        $sql = "select  count(1) cant
                from    tipo_usuario tu,user u
                left    join bitacora_detalle bd on bd.user=u.id
                WHERE   u.tipo=tu.id ".$where;
        $sql .= $group;
        $query = $this->db->query($sql);
        $total = $query->result();


        #************************************************Pagination
        $per_page = 30;
        $url = site_url("reporte/personal_acceso/");
        $data['paging'] = $this->paginate($per_page,$total[0]->cant,3,$url);
        #************************************************Pagination
        */
       
        #************************************************Orden
        $order = "";
        if(empty($order_by)) $order_by=1;
        switch($order_by):
            case 1:
                $order = " order by trim(u.nombre) ";
                break;
            case 2:
                $order = " order by u.email ";
                break;
            case 3:
                $order = " order by tu.tipo ";
                break;
            case 4:
                $order = " order by d.name ";
                break;
            case 5:
                $order = " order by un.unidad ";
                break;
            case 6:
                $order = " order by u.creado ";
                break;
        endswitch;
        if(empty($direccion)) $direccion=1;
        switch($direccion):
            case 1:
                $order .=" asc ";
                break;
            case 2:
                $order .=" desc ";
                break;
        endswitch;
        $order .= " ,bd.date asc ";
        #************************************************Orden

        $sql = "select  ".$campos."
                from    tipo_usuario tu,user u
                left    join bitacora_detalle bd on bd.user=u.id
                WHERE   u.tipo=tu.id ".$where." " . $group . " ".$order;
        if($descargar==0):
            #$sql .= " limit ".$page.",".$per_page;
        endif;
        $query = $this->db->query($sql);
        #$data['rows'] = $query->result();
        $data['rows'] = $array_info = $query->result_array();


        $this->load->model("file_model","model");
        $this->model->set_table("departamento");
        $data['departamentos'] = $this->model->list_rows("*");
        $this->model->set_table("unidad_negocio");
        $data['unidad_negocio'] = $this->model->list_rows("*");
        $this->model->set_table("tipo_usuario");
        $data['tipo_usuario'] = $this->model->list_rows("*");

        if($descargar==0):
            $this->load_header(NULL);
            $this->load->view('control_usuarios',$data);
            $this->load_footer();
        else:
            #Descargamos el archivo en XLS
            $this->load->library('excel');

            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('Control Usuarios');

            #Centramos todas las columnas
            $this->excel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            #NOMBRE DE LAS COLUMNAS
            if($tipo_reporte==1):
                $this->excel->getActiveSheet()->SetCellValue('A1', "Fecha");
                $this->excel->getActiveSheet()->SetCellValue('B1', "Nombre");
                $this->excel->getActiveSheet()->SetCellValue('C1', "Usuario");
                $this->excel->getActiveSheet()->SetCellValue('D1', "Total minutos");
                $this->excel->getActiveSheet()->SetCellValue('E1', "Visitas");
                $this->excel->getActiveSheet()->SetCellValue('F1', "Promedio minutos diarios ");
                $this->excel->getActiveSheet()->SetCellValue('G1', "Detalle Ingresos");
            else:
                $this->excel->getActiveSheet()->SetCellValue('A1', "Nombre");
                $this->excel->getActiveSheet()->SetCellValue('B1', "Usuario");
                $this->excel->getActiveSheet()->SetCellValue('C1', "Total minutos");
                $this->excel->getActiveSheet()->SetCellValue('D1', "Visitas");
                $this->excel->getActiveSheet()->SetCellValue('E1', "Promedio minutos diarios ");
            endif;

            #formateamos el reporte
            foreach($array_info as &$a):
                if(isset($a['horas']) && !empty($a['horas'])):
                    $a['horas'] = str_replace("#@#","\r\n",$a['horas']);
                endif;
            endforeach;

            $this->excel->getActiveSheet()->fromArray($array_info, NULL, 'A2');

            $this->excel->getActiveSheet()->getStyle('G1:G'.$this->excel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);


            #Auto resize
            for($col = 'A'; $col !== 'G'; $col++) {
                $this->excel->getActiveSheet()
                    ->getColumnDimension($col)
                    ->setAutoSize(true);
            }

            #tamaño especial para el detalle horas
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(28.20);

            $filename='control_usuarios.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            $objWriter->save('php://output');
        endif;
    }

    /**
     * @param $per_page
     * @param $total
     * @param $uri_segment
     * @param $url
     * @return mixed
     */
    private function paginate($per_page,$total,$uri_segment,$url){
        $this->load->library('pagination');
        $per_page = $per_page;
        $config['base_url'] = $url;
        $config['total_rows'] = $total;
        $config['per_page'] = $per_page;
        $config["uri_segment"] = $uri_segment;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);

        $config['num_tag_open'] = $config['next_tag_open'] = $config['last_tag_open'] = $config['prev_tag_open'] = $config['first_tag_open'] = '<li>';
        $config['num_tag_close'] = $config['last_tag_close'] = $config['next_tag_close'] = $config['prev_tag_close'] = $config['first_tag_close'] = '</li>';

        $config['cur_tag_open'] = "<li class='active'><a href='#'>";
        $config['cur_tag_close'] = "</li>";

        $this->pagination->initialize($config);

        return $this->pagination->create_links();
    }
#**************************************************************************************************
    public function swap_dates(){
        $this->load->model("file_model","model");
        $this->model->set_table("bitacora");
        $res = $this->model->list_rows("user,TIMESTAMPDIFF( MINUTE, login, logout ) diff  ,login,logout");
        #echo $this->db->last_query();
        $this->model->set_table("bitacora2");
        #echo "<pre>";
        #var_dump($res);
        #echo "</pre>";
        foreach($res as $r):
            echo "as<br>";
            $array = NULL;
            $array = array('user'=>$r->user);
            if($r->diff>0):
                $array['login'] = $r->login;
                $array['logout'] = $r->logout;
            else:
                $array['login'] = $r->logout;
                $array['logout'] = $r->login;
            endif;
            $this->model->insert($array);
        endforeach;
    }
}