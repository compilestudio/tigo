<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Update extends MY_Controller {
#**************************************************************************************************
    public function index($guardo=0,$nodo=""){
        if($nodo=="")
            $nodo = $this->input->post("nodo");
        $data['nodo_seleccionado'] =$nodo;
        $data['guardo'] =$guardo;
        $this->load_header(NULL);

        $sql = "select  *
                from    direcciones
                WHERE   (lat is null
                OR      lon is null) ";
        if(!empty($nodo)):
            $sql .= " and nodo like '".$nodo."' ";
        endif;
        $sql .= " order by direccion asc";

        if(empty($nodo)):
            $sql .= " limit 0,25";
        endif;

        $query = $this->db->query($sql);
        $data['rows'] = $query->result();

        #Obtenemos el listado de los nodos
        /*
        $sql = "SELECT distinct nodo,count(1) cant
                FROM `direcciones`
                where (lat is NULL
                or lon is NULL)
                and nodo in ('AB5843A','AB5844A','AB5942A','AB5944A','AB6043A','AB6143A','AB6242D','CN3648A','CN3651A','CN3749A','CN3749B','CN3849A','CN3850A','CN3850B','CN3952A','CN3953A','CN4052A','CN4146B','CN4152A','CN4153B','CN4156B','CN4254A','CN4255A','CN4451A','JP5655A','JP5656A','JP5753B','JP5754A','JP5754B','JP5758A','JP5858A','JP5955A','JP5956B','JP5957A','JP6057A','JP6154A','JP6155A','JP6254A','JP6353A','JP6354A','JP6655A','JP6754A','MP5135A','MP5236A','MP5238A','MP5336A','MP5337A','MP5338A','MP5435A','MP5536A','MP5537A','MP5735A','MP5737A','MP5838A','MP5847A','MP5947B','MP5948A','MP6048A','MP6147A','PP8448A','PP8548A','PP8549A','PP8550A','PP8649A','PP8748A','PP8749A','PP8750A','PP8750B','PP8751A','PP8752A','PP8848A','PP8850A','PP8850B','PP8851A','PP8852A','PP8853A','PP8854A','PP8949A','PP8949B','PP8950A','PP8951A','PP9050A','PP9050B','PP9051A','PP9052A','PP9054A','PP9055A','PP9149A','PP9151A','PP9151B','PP9249A','PP9251A','PP9151B','PP9252A','PP9351A','PP9353B','PP9449A','RD3542A','RD3543A','RD3543B','RD3543C','RD3544A','RD3638A','RD3640B','RD3641A','RD3642A','RD3643A','RD3643B','RD3643C','RD3643D','RD3644A','RD3737A','RD3739B','RD3743A','RD3744A','RD3833A','RD3837A','RD3837B','RD3838A','RD3838B','RD3839A','RD3839B','RD3840A','RD3841A','RD3936A','RD3937A','RD3937B','RD3938A','RD3939A','RD3940A','RD3941A','RD3943A','RD3944A','RD3944B','RD4035A','RD4035B','RD4037A','RD4038A','RD4038B','RD4038C','RD4039B','RD4040A','RD4040B','RD4136A','RD4136B','RD4136C','RD4137A','RD4137B','RD4137C','RD4138A','RD4139A','RD4233A','RD4236A','RD4236B','RD4236C','RD4237A','RD4237B','RD4238A','RD4238B','RD4239A','RD4239B','RD4240A','RD4240B','RD4241A','RD4334A','RD4334B','RD4336A','RD4337A','RD4338A','RD4339A','RD4339B','RD4339C','RD4432A','RD4434A','RD4434B','RD4435A','RD4436A','RD4437A','RD4438A','RD4533A','RD4534A','RD4535A','RD4536A','RD4537A','RD4537A','RD4538A','RD4637A','RD4638A','RD4639A','RD4640A','RD4734A','RD4735A','RD4735B','RD4736A','RD4737A','RD4737B','RD4738A','RD4834A','RD4835A','RD4835B','RD4836A','RD4838A','RD4838B','RD4935A','RD4936A','RD4937A','RD4939A','RD5035A','RD5035B','RD5035C','RD5037A','RD5136A','RD5137A','RD5138A','RD5238B','RD5752A','TO4554A','TO4652A','TO4652B','TO4676b','TO4656C','TO4654A','TO4747A','TO4849A','TO4850A','TO4853A','TO4853B','TO4950A','TO4952A','TO4954A','TO5079A','TO5051A','TO5052A','TO5053A','TO5149B','TO5150A','TO5152A','TO5155A','TO5250A','TO5250B','TO5253A','TO5254A','TO5350A','TO5351A','TO5444A','TO5451A','TO5453A','TO5550A','TO5551A','TO5551B','TO5649A','TO5649B','TO5650A','TO5650B','TO5651A','TO5748A','TO5749A','TO5740A','TO5753A','TO5849A','TO5852A','TO5945B','TO5956A','TO6156A','TO6257A','TO6553A','TO6557A','TO6557B','TO6657A','TO6658A','TO6758A','TO6760A','TO6854A','TO7455A','TO7557A','TO7657A','TO7757A','TO7856A','TO8256A','TO8757A')
                group by nodo asc";
        */

        #'AG5315A','AG5414A','AG5415A','AG5416A','AG5515A','AG5515B','AG5516A','AG5818A','AG5916B','JP5565A','JP5764A','
        $sql = "SELECT distinct nodo,count(1) cant
                FROM `direcciones`
                where (lat is NULL
                or lon is NULL)
                group by nodo asc";
        $query = $this->db->query($sql);
        $data['nodos'] = $query->result();

        #Sin coordenadas
        $sql = "SELECT count(1) cant
                FROM `direcciones`
                WHERE (lat is NULL
                or lon is NULL)";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0):
            $sin = $query->result();
            $data['sin'] = $sin[0]->cant;
        else:
            $data['sin'] = 0;
        endif;

        #Con coordenadas
        $sql = "SELECT count(1) cant
                FROM `direcciones`
                WHERE (lat is not NULL
                or lon is not NULL)";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0):
            $con = $query->result();
            $data['con'] = $con[0]->cant;
        else:
            $data['con'] = 0;
        endif;



        $this->load->view('sin_coordenadas',$data);
        $this->load_footer();
    }
#**************************************************************************************************
    public function save(){
        $this->load->library('gpoint');
        $ids = $this->input->post("id");
        $lat = $this->input->post("latitud");
        $lon = $this->input->post("longitud");
        $nodo = $this->input->post("nodo");

        $guardo =0;

        for($i=0;$i<count($ids);$i++):
            if(trim($lat[$i])!='' && trim($lon[$i])!=''):
                $this->gpoint->setUTM($lat[$i],$lon[$i],"15N");
                $this->gpoint->convertTMtoLL();

                $data = array(
                    'lat' => $this->gpoint->Lat(),
                    'lon' => $this->gpoint->Long()
                );
                $this->db->where('id',$ids[$i]);
                $this->db->update('direcciones', $data);
                $guardo =1;
            endif;
        endfor;
        redirect("update/".$guardo."/".$nodo);

    }
#**************************************************************************************************
	public function coords(){
        /*
        #Casas
		$sql = "select d.id,count(1) cant,c.lat,c.lon
                from direcciones d,coordenadas c
                where c.nodo=d.nodo
                and casa_no=casa_lote
                and (d.lat='' or d.lat is null)
                and d.id>119415
                group by c.nodo,c.casa_lote
                having cant=1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0):
            foreach ($query->result() as $row):
                $data = array(
                    'lat' => $row->lat,
                    'lon' => $row->lon
                );
                $this->db->where('id', $row->id);
                $this->db->update('direcciones', $data);
            endforeach;
        endif;
/*
        #Lotes
        $sql = "select d.id,count(1) cant,c.lat,c.lon
                from direcciones d,coordenadas c
                where c.nodo=d.nodo
                and (lote=casa_lote or
                concat('L',lote)=casa_lote)
                and (d.lat='' or d.lat is null)
                and d.id>119415
                group by c.nodo,c.casa_lote
                having cant=1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0):
            foreach ($query->result() as $row):
                $data = array(
                    'lat' => $row->lat,
                    'lon' => $row->lon
                );
                $this->db->where('id', $row->id);
                $this->db->update('direcciones', $data);
            endforeach;
        endif;
*/
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */