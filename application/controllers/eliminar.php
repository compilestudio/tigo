<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eliminar extends MY_Controller {
#**************************************************************************************************
    public function direcciones($op=0){
        $this->load_header(NULL);

        #Obtenemos el listado de los nodos
        $sql = "SELECT distinct nodo
                FROM `direcciones`";
        $query = $this->db->query($sql);
        $data['nodos'] = $query->result();


        #We get the post variables
        $nodo_ = $this->input->post("nodo");
        $search = $this->input->post("search");
        $data['nodo_'] = $nodo_;
        $data['search_'] = $search;
        $data['op'] = $op;

        if(!empty($nodo_)):
            $cond = $this->condicion($search);
            $sql = "select  *
                    from    direcciones
                    WHERE   nodo like '%".$nodo_."%' ".$cond;
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0):
                $data['rows'] = $query->result();
            else:
                $data['rows'] = NULL;
            endif;
        endif;

        $this->load->view('eliminar_direcciones',$data);
        $this->load_footer();
    }
#**************************************************************************************************
    public function condicion($term){
        //$term = $this->input->get('term');

        $t = explode(" ",strtolower(trim($term)));

        $cond = "";

        $reservadas = array('poste','boulevard','avenida','calle','callejon','calzada','carretera','kilometro','diagonal','via','ruta','num','contador','zona','edificio','nivel','apartamento','colonia','condominio','residencial','urbanizacion','comercio','comercial','institucion','local','oficina','plaza','lote','cluster','condado','manzana','proyecto','sector','fase','tronco','finca','aldea','caserio','canton','departamento','municipio','operador','red','localizacion','catastral','granja','anexo','fraccion','interior','modulo','cerro','torre','kiosko','parcela','salon','referencia','restaurante','barrio','pasaje','mercado','asentamiento','eje','lotificacion','seccion');

        for($i=0;$i<count($t);$i++):
            $t1=strtolower($t[$i]);

            $prev = "";

            if($i>0):
                $prev = $t[$i-1];
            endif;

            if(($i+1)<count($t) && in_array($t[$i],$reservadas)):
                $search = "";
                if(!in_array($t[$i+1],$reservadas)):
                    #incrementamos el contador
                    $i++;
                    #Seteamos el primer termino a search
                    $search = $t[$i];
                    #Mientras no este el close search
                    $i++;
                    while($i<count($t) && !in_array($t[$i],$reservadas)):
                        $search .= " ".$t[$i];
                        $i++;
                    endwhile;
                    if($i==(count($t)-1) && isset($t[$i])):
                        $search .= " ".$t[$i];
                    else:
                        $i--;
                    endif;
                else:
                    $search = $t[$i+1];
                endif;

                #echo trim($search,'"').PHP_EOL;
                #$search = trim($search,'"');


                #Armamos las condiciones en base a lo que viene en la descripcion
                if(isset($search) && !empty($search)):

                    if($t1=="boulevard" || $t1=="avenida" || $t1=="calzada"):
                        #Boulevard
                        $cond .= " and (boulevard like '".$search."%' ";
                        #Avenida
                        if(in_array($search,$reservadas)):
                            $cond .= " or avenida like '".$prev."%'";
                        else:
                            $cond .= " or (avenida like '".$search."%' or avenida like '".$prev." ".$search."')";
                        endif;
                        #Calzada
                        $cond .= " or calzada like '".$search."%') ";
                    endif;

                    if($t1=="callejon" || $t1=="pasaje"):
                        $cond .= " and (callejon like '".$search."%' or pasaje like '".$search."%') ";
                    endif;

                    if($t1=="colonia" || $t1=="condominio" || $t1=="residencial" || $t1=="urbanizacion" || $t1=="barrio"):
                        $cond .= " and (colonia like '".$search."%' ";
                        $cond .= " or condominio like '".$search."%' ";
                        $cond .= " or residencial like '".$search."%' ";
                        $cond .= " or urbanizacion like '".$search."%' ";
                        $cond .= " or barrio like '".$search."%') ";
                    endif;

                    if($t1=="comercio" || $t1=="mercado" || $t1=="comercial" || $t1=="plaza"):
                        $cond .= " and comercio like '".$search."%' ";
                        $cond .= " and mercado like '".$search."%' ";
                        $cond .= " and c_comercial like '".$search."%' ";
                        $cond .= " and plaza like '".$search."%' ";
                    endif;

                    if($t1=="proyecto" || $t1=="asentamiento"):
                        $cond .= " and (proyecto like '".$search."%' ";
                        $cond .= " or asentamiento like '".$search."%') ";
                    endif;

                    if($t1=="sector" || $t1=="lote" || $t1=="manzana"):
                        $cond .= " and (sector like '".$search."%' ";
                        $cond .= " or lote like '".$search."%' ";
                        $cond .= " or manzana like '".$search."%') ";
                    endif;

                    if($t1=="aldea" || $t1=="canton" || $t1=="caserio"):
                        $cond .= " and (aldea like '".$search."%' ";
                        $cond .= " or canton like '".$search."%' ";
                        $cond .= " or caserio like '".$search."%') ";
                    endif;

                    if($t1=="ruta" || $t1=="carretera"):
                        $cond .= " and (ruta like '".$search."%' ";
                        $cond .= " or carretera like '".$search."%') ";
                    endif;

                    if($t1=="parcela" || $t1=="finca"):
                        $cond .= " and (parcela like '".$search."%' ";
                        $cond .= " or finca like '".$search."%') ";
                    endif;


                    switch($t1):
                        case "poste":
                            $cond .= " and poste like '".$search."%' ";
                            break;
                        case "calle":
                            if(in_array($search,$reservadas)):
                                $cond .= " and calle like '".$prev."%'";
                            else:
                                $cond .= " and (calle like '".$search."%' or calle like '".$prev." ".$search."')";
                            endif;
                            break;
                        case "kilometro":
                            $cond .= " and kilometro like '".$search."%' ";
                            break;
                        case "diagonal":
                            $cond .= " and diagonal like '".$search."%' ";
                            break;
                        case "via":
                            $cond .= " and via like '".$search."%' ";
                            break;
                        case "num":
                            $cond .= " and casa_no like '".$search."%' ";
                            break;
                        case "contador":
                            $cond .= " and contador like '".$search."%' ";
                            break;
                        case "zona":
                            $cond .= " and zona like '".$search."%' ";
                            break;
                        case "edificio":
                            $cond .= " and edificio like '".$search."%' ";
                            break;
                        case "nivel":
                            $cond .= " and nivel like '".$search."%' ";
                            break;
                        case "apartamento":
                            $cond .= " and apartamento like '".$search."%' ";
                            break;
                        case "institucion":
                            $cond .= " and institucion like '".$search."%' ";
                            break;
                        case "local":
                            $cond .= " and no_local like '".$search."%' ";
                            break;
                        case "oficina":
                            $cond .= " and oficina like '".$search."%' ";
                            break;
                        case "cluster":
                            $cond .= " and cluster like '".$search."%' ";
                            break;
                        case "condado":
                            $cond .= " and condado like '".$search."%' ";
                            break;
                        case "fase":
                            $cond .= " and fase like '".$search."%' ";
                            break;
                        case "tronco":
                            $cond .= " and tronco like '".$search."%' ";
                            break;
                        case "departamento":
                            $cond .= " and ciudad like '".$search."%' ";
                            break;
                        case "municipio":
                            $cond .= " and municipio like '".$search."%' ";
                            break;
                        case "operador":
                            $cond .= " and operador like '".$search."%' ";
                            break;
                        case "red":
                            $cond .= " and red like '".$search."%' ";
                            break;
                        case "localizacion":
                            $cond .= " and localizacion like '".$search."%' ";
                            break;
                        case "catastral":
                            $cond .= " and catastral like '".$search."%' ";
                            break;
                        case "granja":
                            $cond .= " and granja like '".$search."%' ";
                            break;
                        case "anexo":
                            $cond .= " and anexo like '".$search."%' ";
                            break;
                        case "fraccion":
                            $cond .= " and fraccion like '".$search."%' ";
                            break;
                        case "interior":
                            $cond .= " and interior like '".$search."%' ";
                            break;
                        case "modulo":
                            $cond .= " and modulo like '".$search."%' ";
                            break;
                        case "cerro":
                            $cond .= " and cerro like '".$search."%' ";
                            break;
                        case "torre":
                            $cond .= " and torre like '".$search."%' ";
                            break;
                        case "kiosko":
                            $cond .= " and kiosko like '".$search."%' ";
                            break;
                        case "salon":
                            $cond .= " and salon like '".$search."%' ";
                            break;
                        case "referencia":
                            $cond .= " and referencia like '".$search."%' ";
                            break;
                        case "restaurante":
                            $cond .= " and restaurante like '".$search."%' ";
                            break;
                        case "eje":
                            $cond .= " and eje like '".$search."%' ";
                            break;
                        case "lotificacion":
                            $cond .= " and lotificacion like '".$search."%' ";
                            break;
                        case "seccion":
                            $cond .= " and seccion like '".$search."%' ";
                            break;
                    endswitch;
                endif;
            elseif(($i+1)==count($t) && in_array($t[$i],$reservadas)):
                if($t[$i]=="avenida"):
                    $cond .= " and avenida like '".$prev."'";
                endif;
                if($t[$i]=="calle"):
                    $cond .= " and calle like '".$prev."'";
                endif;
            endif;
        endfor;

        return $cond;
    }
#**************************************************************************************************
    public function uno(){
        $id = $this->input->post("id");
        $this->load->model("file_model","model");
        $this->model->set_table("direcciones");

        $this->model->delete($id);

    }
#**************************************************************************************************
    public function masivo(){
        $this->load->model("file_model","model");
        $this->model->set_table("direcciones");

        $id = $this->input->post("id");

        foreach($id as $i):
            $this->model->delete($i);
        endforeach;

        redirect("eliminar/direcciones/1");
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */