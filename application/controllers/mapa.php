<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mapa extends MY_Controller {

#**************************************************************************************************
    public function index($padre=0){
        $this->load_header(NULL);
        $this->load->model("file_model","model");
        $this->model->set_table("direcciones");
        $data['deptos'] = $this->model->list_rows("distinct(departamento) depto","","departamento asc");

        #Obtenemos el listado de los nodos
        $sql = "SELECT distinct nodo
                FROM `direcciones`";
        $query = $this->db->query($sql);
        $data['nodos'] = $query->result();


        $this->load->view('mapa',$data);    
        $this->load_footer();
    }
#**************************************************************************************************
    public function get_municipios(){
        $depto = $this->input->post("dep");

        $this->load->model("file_model","model");
        $this->model->set_table("direcciones");
        $municipios = $this->model->list_rows("distinct(municipio) municipio","departamento='" . $depto . "'","departamento asc");
        echo "<option value='-1'>Municipio</option>";
        if(!empty($municipios))
        foreach($municipios as $m):
            echo "<option>".$m->municipio."</option>";
        endforeach;
    }
#**************************************************************************************************
    public function get_zonas(){
        $depto = $this->input->post("dep");
        $mun = $this->input->post("mun");

        $this->load->model("file_model","model");
        $this->model->set_table("direcciones");
        $zonas = $this->model->list_rows("distinct(zona) zona","departamento='" . $depto . "' and municipio='" . $mun . "'", "CAST(zona as SIGNED INTEGER) ASC");
        echo "<option value='-1'>Zona</option>";
        if(!empty($zonas))
        foreach($zonas as $z):
            echo "<option>".$z->zona."</option>";
        endforeach;
    }
#**************************************************************************************************
    public function search(){
        $id = $this->input->post("id");

        $this->db->select('lat,lon,nodo,observaciones');
        if(ctype_digit($id)!==false):
            $this->db->where(array('id' => $id));
        else:
            $this->db->where(array('nodo' => $id));
        endif;
        $query = $this->db->get('direcciones',1,0);

        $result = $query->result();

        header('Content-Type: application/json');
        echo json_encode($result);
    }
#**************************************************************************************************
    public function poste(){
        $id = $this->input->post("poste");

        $this->db->select('lat,lon');
        $this->db->where(array('cast(numero as char) = ' => trim($id)));
        $query = $this->db->get('poste');

        if ($query->num_rows() > 0):
            $result = $query->result();

            header('Content-Type: application/json');
            echo json_encode($result);
        endif;
    }
#**************************************************************************************************
    public function loadExcel(){

        $this->load->helper('file');
        $this->load->helper('path');
        $this->load->model("file_model","file");

        $path = set_realpath("_direcciones");
        $files = get_filenames($path);

        #for($k=0;$k<count($files);$k++):
        for($k=0;$k<1;$k++):
            $this->file->processExcel($path,$files[$k],$k);
        endfor;

    }
#**************************************************************************************************
    public function loadCordenadas(){

        $this->load->helper('file');
        $this->load->helper('path');
        $this->load->model("file_model","file");

        $path = set_realpath("_coordenadas");
        $files = get_filenames($path);

        #for($k=0;$k<count($files);$k++):
        for($k=0;$k<1;$k++):
            $this->file->processCoordenadas($path,$files[$k],$k);
        endfor;

    }
#**************************************************************************************************
    public function autocompletar_previa(){
        $term = $this->input->get('term');

        $sql = "SELECT  direccion label,id
                FROM    direcciones
                WHERE   lat!=''
                and     MATCH(direccion) AGAINST ('".$term."')
                order by MATCH(direccion) AGAINST ('".$term."') desc
                limit 0,10";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0){
            $result = $query->result();
            header('Content-Type: application/json');
            echo json_encode($result);
        }
    }
#**************************************************************************************************
    public function autocompletar(){
        $term = $this->input->get('term');
        $term = str_replace('Ñ','N',str_replace('ñ','n',$term));

        $t = explode(" ",strtolower(trim($term)));
        $cond = "";
        $join ="";

        $reservadas = array('poste','boulevard','avenida','calle','callejon','calzada','carretera','kilometro','diagonal','via','ruta','num','contador','zona','edificio','nivel','apartamento','colonia','condominio','residencial','urbanizacion','comercio','comercial','institucion','local','oficina','plaza','lote','cluster','condado','manzana','proyecto','sector','fase','tronco','finca','aldea','caserio','canton','departamento','municipio','operador','red','localizacion','catastral','granja','anexo','fraccion','interior','modulo','cerro','torre','kiosko','parcela','salon','referencia','restaurante','barrio','pasaje','mercado','asentamiento','eje','lotificacion','seccion','nodo');

        $first_col = "d.direccion";
        $limit = "0,10";

        for($i=0;$i<count($t);$i++):
            $t1=strtolower($t[$i]);

            $prev = "";

            if($i>0):
                $prev = $t[$i-1];
            endif;

            if(($i+1)<count($t) && in_array($t[$i],$reservadas)):
                $search = "";
                if(!in_array($t[$i+1],$reservadas)):
                    #incrementamos el contador
                    $i++;
                    #Seteamos el primer termino a search
                    $search = $t[$i];
                    #Mientras no este el close search
                    $i++;
                    while($i<count($t) && !in_array($t[$i],$reservadas)):
                        $search .= " ".$t[$i];
                        $i++;
                    endwhile;
                    if($i==(count($t)-1) && isset($t[$i])):
                        $search .= " ".$t[$i];
                    else:
                        $i--;
                    endif;
                else:
                    $search = $t[$i+1];
                endif;



                #echo trim($search,'"').PHP_EOL;
                #$search = trim($search,'"');
                #Armamos las condiciones en base a lo que viene en la descripcion
                if(isset($search) && !empty($search)):

                    if($t1=="boulevard" || $t1=="avenida" || $t1=="calzada"):
                        #Boulevard
                        $cond .= " and (boulevard like '".$search."%' ";
                        #Avenida
                        if(in_array($search,$reservadas)):
                            $cond .= " or avenida like '".$prev."%'";
                        else:
                            $cond .= " or (avenida like '".$search."%' or avenida like '".$prev." ".$search."')";
                        endif;
                        #Calzada
                        $cond .= " or calzada like '".$search."%') ";
                    endif;

                    if($t1=="callejon" || $t1=="pasaje"):
                        $cond .= " and (callejon like '".$search."%' or pasaje like '".$search."%') ";
                    endif;

                    if($t1=="colonia" || $t1=="condominio" || $t1=="residencial" || $t1=="urbanizacion" || $t1=="barrio"):
                        $cond .= " and (colonia like '".$search."%' ";
                        $cond .= " or condominio like '".$search."%' ";
                        $cond .= " or residencial like '".$search."%' ";
                        $cond .= " or urbanizacion like '".$search."%' ";
                        $cond .= " or barrio like '".$search."%') ";
                    endif;

                    if($t1=="comercio" || $t1=="mercado" || $t1=="comercial" || $t1=="plaza"):
                        $cond .= " and (comercio like '".$search."%' ";
                        $cond .= " or mercado like '".$search."%' ";
                        $cond .= " or c_comercial like '".$search."%' ";
                        $cond .= " or plaza like '".$search."%' ) ";
                    endif;

                    if($t1=="proyecto" || $t1=="asentamiento"):
                        $cond .= " and (proyecto like '".$search."%' ";
                        $cond .= " or asentamiento like '".$search."%') ";
                    endif;

                    if($t1=="sector" || $t1=="lote" || $t1=="manzana"):
                        $cond .= " and (sector like '".$search."%' ";
                        $cond .= " or lote like '".$search."%' ";
                        $cond .= " or manzana like '".$search."%') ";
                    endif;

                    if($t1=="aldea" || $t1=="canton" || $t1=="caserio"):
                        $cond .= " and (aldea like '".$search."%' ";
                        $cond .= " or canton like '".$search."%' ";
                        $cond .= " or caserio like '".$search."%') ";
                    endif;

                    if($t1=="ruta" || $t1=="carretera"):
                        $cond .= " and (ruta like '".$search."%' ";
                        $cond .= " or carretera like '".$search."%') ";
                    endif;

                    if($t1=="parcela" || $t1=="finca"):
                        $cond .= " and (parcela like '".$search."%' ";
                        $cond .= " or finca like '".$search."%') ";
                    endif;


                    switch($t1):
                        case "poste":
                            $cond .= " and poste like '".$search."%' ";
                            break;
                        case "calle":
                            if(in_array($search,$reservadas)):
                                $cond .= " and calle like '".$prev."%'";
                            else:
                                $cond .= " and (calle like '".$search."%' or calle like '".$prev." ".$search."')";
                            endif;
                            break;
                        case "kilometro":
                            $cond .= " and kilometro like '".$search."%' ";
                            break;
                        case "diagonal":
                            $cond .= " and diagonal like '".$search."%' ";
                            break;
                        case "via":
                            $cond .= " and via like '".$search."%' ";
                            break;
                        case "num":
                            $cond .= " and casa_no like '".$search."%' ";
                            break;
                        case "contador":
                            $cond .= " and contador like '".$search."%' ";
                            break;
                        case "zona":
                            $cond .= " and zona like '".$search."%' ";
                            break;
                        case "edificio":
                            $cond .= " and edificio like '".$search."%' ";
                            break;
                        case "nivel":
                            $cond .= " and nivel like '".$search."%' ";
                            break;
                        case "apartamento":
                            $cond .= " and apartamento like '".$search."%' ";
                            break;
                        case "institucion":
                            $cond .= " and institucion like '".$search."%' ";
                            break;
                        case "local":
                            $cond .= " and no_local like '".$search."%' ";
                            break;
                        case "oficina":
                            $cond .= " and oficina like '".$search."%' ";
                            break;
                        case "cluster":
                            $cond .= " and cluster like '".$search."%' ";
                            break;
                        case "condado":
                            $cond .= " and condado like '".$search."%' ";
                            break;
                        case "fase":
                            $cond .= " and fase like '".$search."%' ";
                            break;
                        case "tronco":
                            $cond .= " and tronco like '".$search."%' ";
                            break;
                        case "departamento":
                            $cond .= " and dp.depto like '".$search."%' ";
                            $join = " left join municipios m on m.id=d.municipio left join deptos dp on dp.id=m.id_depto ";
                            break;
                        case "municipio":
                            $cond .= " and m.municipio like '".$search."%' ";
                            $join = " left join municipios m on m.id=d.municipio left join deptos dp on dp.id=m.id_depto ";
                            break;
                        case "operador":
                            $cond .= " and operador like '".$search."%' ";
                            break;
                        case "red":
                            $cond .= " and red like '".$search."%' ";
                            break;
                        case "localizacion":
                            $cond .= " and localizacion like '".$search."%' ";
                            break;
                        case "catastral":
                            $cond .= " and catastral like '".$search."%' ";
                            break;
                        case "granja":
                            $cond .= " and granja like '".$search."%' ";
                            break;
                        case "anexo":
                            $cond .= " and anexo like '".$search."%' ";
                            break;
                        case "fraccion":
                            $cond .= " and fraccion like '".$search."%' ";
                            break;
                        case "interior":
                            $cond .= " and interior like '".$search."%' ";
                            break;
                        case "modulo":
                            $cond .= " and modulo like '".$search."%' ";
                            break;
                        case "cerro":
                            $cond .= " and cerro like '".$search."%' ";
                            break;
                        case "torre":
                            $cond .= " and torre like '".$search."%' ";
                            break;
                        case "kiosko":
                            $cond .= " and kiosko like '".$search."%' ";
                            break;
                        case "salon":
                            $cond .= " and salon like '".$search."%' ";
                            break;
                        case "referencia":
                            $cond .= " and referencia like '".$search."%' ";
                            break;
                        case "restaurante":
                            $cond .= " and restaurante like '".$search."%' ";
                            break;
                        case "eje":
                            $cond .= " and eje like '".$search."%' ";
                            break;
                        case "lotificacion":
                            $cond .= " and lotificacion like '".$search."%' ";
                            break;
                        case "seccion":
                            $cond .= " and seccion like '".$search."%' ";
                            break;
                        case "nodo":
                            $cond .= " and nodo like '".$search."%' ";
                            $first_col = "d.nodo";
                            $limit = "0,1";
                            break;
                    endswitch;
                endif;
            elseif(($i+1)==count($t) && in_array($t[$i],$reservadas)):
                if($t[$i]=="avenida"):
                    $cond .= " and avenida like '".$prev."'";
                endif;
                if($t[$i]=="calle"):
                    $cond .= " and calle like '".$prev."'";
                endif;
            endif;
        endfor;

        if(!empty($cond)):
            $dep = $this->input->get("departamento");
            $mun = $this->input->get("municipio");
            $zona = $this->input->get("zona");
            $cond2 = "";
            if(!empty($dep) && $dep!=-1):
                $cond2 .= " and departamento like '%".$dep."%' ";
            endif;
            if(!empty($mun) && $mun!=-1):
                $cond2 .= " and municipio like '%".$mun."%' ";
            endif;
            if(!empty($zona) && $zona!=-1):
                $cond2 .= " and zona = '".$zona."' ";
            endif;
            $sql = "SELECT  ".$first_col." label,d.id
                    FROM    direcciones d ".$join."
                    WHERE   (lat!='' and lat !=0 and lat is not null) ".$cond." ".$cond2."
                    order by direccion asc
                    limit ".$limit;
            header('Content-Type: text/html; charset=utf-8');
            #echo $sql;
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0):
                $result = $query->result();
                header('Content-Type: application/json');
                echo json_encode($result);
                return;
            endif;
        endif;
        echo json_encode(FALSE);
    }
#**************************************************************************************************
    public function salvar_usuario(){
        $array = array(
            'nombre' => $this->input->post("nombre"),
            'direccion' => $this->input->post("direccion"),
            'email' => $this->input->post("email"),
            'telefono' => $this->input->post("telefono"),
            'longitud' => $this->input->post("longitud"),
            'latitud' => $this->input->post("latitud"),
            'comentarios' => $this->input->post("comentarios"),
            'user' => $this->session->userdata('user_tigo')
        );

        $dir = $this->input->post("direccion");

        if(!empty($dir)):
            $this->load->model("file_model","file");
            $this->file->set_table("registro_cliente");

            $this->file->insert($array);
        endif;
    }
#**************************************************************************************************
    public function validate_user(){

        $pwd=$this->input->post("pass");
        $error = "";

        if( strlen($pwd) > 20 ) {
            $error .= "Muy larga<br>";
        }

        if( strlen($pwd) < 8 ) {
            $error .= "Mínimo 8 caracteres<br>";
        }

        if( !preg_match("#[0-9]+#", $pwd) ) {
            $error .= "Por lo menos un número<br>";
        }


        if( !preg_match("#[a-z]+#", $pwd) ) {
            $error .= "Por lo menos una letra<br>";
        }


        if( !preg_match("#[A-Z]+#", $pwd) ) {
            $error .= "Por lo menos una mayúscula!<br>";
        }



        if( !preg_match("#\W+#", $pwd) ) {
            $error .= "Por lo menos un símbolo!<br>";
        }


        if(!empty($error)) {
            echo $error;
        }else{
            echo NULL;
        }
    }
#**************************************************************************************************
    public function change_pass(){
        $contrasena = $this->input->post("contrasena");
        $conf = $this->input->post("conf");
        $this->load->library('encrypt');

        if($contrasena == $conf){
            $array = array(
                'password' => $this->encrypt->encode($conf),
                'force' => 0
            );
            $this->db->where('id',$this->session->userdata('user_tigo'));
            $this->db->update('user', $array);
            $this->session->set_userdata('tigo_force', 0);
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */