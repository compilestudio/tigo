<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Fecha Creación: 2011-dic-21
 * Autor: Víctor López
 * Fecha última modificación: 2012-mar-13
 * Último en modificarla:  Pedro Izaguirre
 * 
 * Descripción: 
 * */

class Session extends MY_Controller {
	
	public function index() {
		
		redirect('session/start');
		
	}
#We start the session	
	public function start() {
		
		$email = $this->input->post('user_name');
		$password = $this->input->post('password');
		
		#si recibimos datos via post
		if ($email && $password):
		
			#cargamos el modelo de sesión
			$this->load->model('session_model');
			#si el usuario está autorizado 
			if ($this->session_model->authorized($email, $password)) :
				#si el usuario pidió que se recordara su sesión
				if ($this->input->post('remember')) :
					$this->session_model->remember();
				endif;

				redirect('mapa');
			endif;
			
		endif;
		redirect('landing/1');
				
	}
	
	public function display() {
	
		echo '<pre>' . print_r($this->session->all_userdata(), true) . '</pre>';
		
	}
	#End the session
	public function end() {

		$this->save_logoff();

		$this->session->sess_destroy();

		redirect('landing');
	}
	public function save_logoff(){
		$user = $this->session->userdata('user_tigo');
		$sql = "SELECT TIMESTAMPDIFF(MINUTE,logout,NOW()) minutes,b.*
					FROM `bitacora` b
					where   user=".$user."
					order by login desc";
		$query = $this->db->query($sql);
		$res = $query->result();
		if ($res[0]->minutes > 10):
			$sql = "update  bitacora
							set 	logout = '".date('Y-m-d G:i:s')."'
							where   user=".$user."
							and     logout='0000-00-00 00:00:00'";
			$this->db->query($sql);
		else:
			$sql = "update  bitacora
							set 	logout = '".date('Y-m-d G:i:s')."'
							where   user=".$user."
							and     logout='".$res[0]->logout."'
							and     login='".$res[0]->login."'";
			$this->db->query($sql);
		endif;
	}
	
} 