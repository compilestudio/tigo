<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Archivos extends MY_Controller {
#**************************************************************************************************
    public function direcciones(){
        $this->load_header(NULL);
        $this->load->view('archivo_direcciones');
        $this->load_footer();
    }
#******************************************************************************************************************************************************************
    #Funcion par aguardar el archivo subido en la sección de solo imágenes
    public function upload_file(){
        $this->load->helper('path');
        $tempFile = $_FILES['Filedata']['tmp_name'];
        $targetPath = set_realpath("_uploads");

        #$file = uniqid() .".". pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);;
        $file = $_FILES['Filedata']['name'];

        $targetFile =  str_replace('//','/',$targetPath) . $file;
        move_uploaded_file($tempFile,$targetFile);

        echo $file;
    }
#******************************************************************************************************************************************************************
    #Funcion par aguardar el archivo subido en la sección de solo imágenes
    public function upload_file_kml(){
        $this->load->helper('path');
        $tempFile = $_FILES['Filedata']['tmp_name'];
        $targetPath = set_realpath("_kml");

        #$file = uniqid() .".". pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);;
        #$file = $_FILES['Filedata']['name'];
        $file = "tigostar.kml";

        $targetFile =  str_replace('//','/',$targetPath) . $file;
        move_uploaded_file($tempFile,$targetFile);

        echo $file;
    }
#******************************************************************************************************************************************************************
    #Funcion par aguardar el archivo kml de la red adquirida
    public function upload_file_kml_adquirida(){
        $this->load->helper('path');
        $tempFile = $_FILES['Filedata']['tmp_name'];
        $targetPath = set_realpath("_kml");

        #$file = uniqid() .".". pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);;
        #$file = $_FILES['Filedata']['name'];
        $file = "adquirida.kml";

        $targetFile =  str_replace('//','/',$targetPath) . $file;
        move_uploaded_file($tempFile,$targetFile);

        echo $file;
    }
#******************************************************************************************************************************************************************
    #Funcion par aguardar el archivo subido en la sección de solo imágenes
    public function upload_file_colonias(){
        $this->load->helper('path');
        $tempFile = $_FILES['Filedata']['tmp_name'];
        $targetPath = set_realpath("_uploads");

        #$file = uniqid() .".". pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);;
        #$file = $_FILES['Filedata']['name'];
        $file = "colonias.xlsx";

        $targetFile =  str_replace('//','/',$targetPath) . $file;
        move_uploaded_file($tempFile,$targetFile);

        echo $file;
    }
#******************************************************************************************************************************************************************
    #Funcion para procesar el archivo
    public function process_file(){
        $this->load->helper('path');

        $file = $this->input->post("file");
        $path = set_realpath("_uploads");
        $this->load->model("file_model","file");

        $this->file->processExcel($path,$file);
    }
#**************************************************************************************************
    public function autocad(){
        $this->load_header(NULL);
        $this->load->view('autocad');
        $this->load_footer();
    }
#******************************************************************************************************************************************************************
    #Funcion para procesar el archivo dxf (autocad texto)
    public function process_dxf(){
        $dxf = $this->input->post("file");
        $this->load->library('gpoint');

        #obtenemos el nombre del nodo del nombre del archivo
        $nodo = explode(".",$dxf);
        $nodo=$nodo[0];


        $this->load->helper('path');
        $this->load->helper('file');
        $path = set_realpath("_uploads");
        $this->load->model("file_model","file");

        #$data = read_file($path."test.dxf");

        $cont=0;

        $array = array('casa_lote'=>0,'lat'=>0,'lon'=>0,'nodo'=>$nodo);
        $file = fopen($path.$dxf, "r");
        while(!feof($file)){
            $line = fgets($file);
            $line = strtolower(trim($line));
            if($line=="acdbentity"):
                $line2 = fgets($file);
                $line2 = strtolower(trim($line2));
                if($line2=="8"):
                    $line3 = fgets($file);
                    $line3 = strtolower(trim($line3));
                    if(strpos($line3, "catastro numeral")!==false):
                        $line4 = fgets($file);
                        $line4 = strtolower(trim($line4));
                        if($line4=="62"):
                            $cont++;
                            #Si llega aquí es porque es una coordenada
                            #Quitamos las 4 lineas que no sirven
                            fgets($file);fgets($file);fgets($file);fgets($file);
                            #obtenemos x
                            $x = fgets($file);
                            #quitamos la otra linea
                            fgets($file);
                            #obtenemos y
                            $y = fgets($file);
                            #quitamos las 6 que siguen que no sirven
                            fgets($file);fgets($file);fgets($file);fgets($file);fgets($file);
                            #Obtenemos el número
                            $array['casa_lote'] = trim(fgets($file));

                            #Convertimos x,y a lat, long
                            $this->gpoint->setUTM($x,$y,"15N");
                            $this->gpoint->convertTMtoLL();
                            $array['lat'] = $this->gpoint->Lat();
                            $array['lon'] = $this->gpoint->Long();



                            #var_dump($array);
                            #echo "<br>";
                            $this->db->insert('coordenadas', $array);
                        endif;
                    endif;
                endif;
            endif;
            #echo $line."<br>";
        }
        echo $cont;
        fclose($file);

    }
#******************************************************************************************************************************************************************
    #Funcion para procesar el archivo dxf (autocad texto)
    public function process_dxf_postes(){
        #$dxf = $this->input->post("file");
        $this->load->library('gpoint');

        #obtenemos el nombre del nodo del nombre del archivo
        #$nodo = explode(".",$dxf);
        #$nodo=$nodo[0];


        $this->load->helper('path');
        $this->load->helper('file');
        $path = set_realpath("_uploads");
        $this->load->model("file_model","file");


        $this->file->set_table("poste");

        $data = read_file($path);

        $cont=0;

        $array = array('numero'=>0,'lat'=>0,'lon'=>0,'x'=>0,'y'=>0);
        $file = fopen($path."test.dxf", "r");
        while(!feof($file)){
            $line = fgets($file);
            $line = strtolower(trim($line));
            echo $line."<br>";
            if($line=="acdbentity"):
                $line2 = fgets($file);
                $line2 = strtolower(trim($line2));
                echo $line2."<br>";
                if($line2=="8"):
                    $line3 = fgets($file);
                    $line3 = strtolower(trim($line3));
                    echo $line3."<br>";
                    if(strpos($line3, "ejes de calle")!==false):
                        $line4 = fgets($file);
                        $line4 = strtolower(trim($line4));
                        if($line4=="62"):
                            $cont++;
                            #Si llega aquí es porque es una coordenada
                            #Quitamos las 4 lineas que no sirven
                            fgets($file);fgets($file);fgets($file);fgets($file);
                            #obtenemos x
                            $x = fgets($file);
                            #quitamos la otra linea
                            fgets($file);
                            #obtenemos y
                            $y = fgets($file);
                            #quitamos las 6 que siguen que no sirven
                            fgets($file);fgets($file);fgets($file);fgets($file);fgets($file);
                            #Obtenemos el número
                            $array['numero'] = trim(fgets($file));

                            #Convertimos x,y a lat, long
                            $this->gpoint->setUTM($x,$y,"15N");
                            $this->gpoint->convertTMtoLL();
                            $array['lat'] = $this->gpoint->Lat();
                            $array['lon'] = $this->gpoint->Long();
                            $array['x'] = $x;
                            $array['y'] = $y;



                            #var_dump($array);
                            #echo "<br>";


                                if($array['numero']>0 && strlen($array['numero'])>1):
                                    $existe = $this->file->list_rows('*','numero='.$array['numero']);
                                    if(empty($existe))
                                        $this->db->insert('poste', $array);
                                endif;
                        endif;
                    endif;
                endif;
            endif;
            #echo $line."<br>";
        }
        echo $cont;
        fclose($file);

    }
#**************************************************************************************************
#**************************************************************************************************
#**************************************************************************************************
#**************************************************************************************************
    public function cargar_archivo_update($nodo_="",$cant=0){
        $this->load_header(NULL);

        $data['nodo_'] = $nodo_;
        $data['cant'] = $cant;

        #Obtenemos el listado de los nodos
        $sql = "SELECT distinct nodo,count(1) cant
                FROM `direcciones`
                where lat is NULL
                or lon is NULL
                group by nodo";
        $query = $this->db->query($sql);
        $data['nodos'] = $query->result();

        $this->load->view('cargar_archivo_update',$data);
        $this->load_footer();
    }
#**************************************************************************************************
    public function guardar_update(){
        $reservadas = array('poste','boulevard','avenida','calle','callejon','calzada','carretera','kilometro','diagonal','via','ruta','num','contador','zona','edificio','nivel','apartamento','colonia','condominio','residencial','urbanizacion','comercio','comercial','institucion','local','oficina','plaza','lote','cluster','condado','manzana','proyecto','sector','fase','tronco','finca','aldea','caserio','canton','departamento','municipio','operador','red','localizacion','catastral','granja','anexo','fraccion','interior','modulo','cerro','torre','kiosko','parcela','salon','referencia','restaurante','barrio','pasaje','mercado','asentamiento','eje','lotificacion','seccion');

        #$nodo = $this->input->post("nodo");
        $filename = $this->input->post("filename");

        $nodo = explode(".",$filename);
        $nodo = explode(" ",$nodo[0]);
        $nodo = $nodo[0];

        #Contabilizamos cuantos quedan antes
        $sql = "SELECT count(1) cant
                FROM `direcciones`
                where (lat is NULL
                or lon is NULL)
                and trim(nodo) = '".$nodo."'
                group by nodo";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0):
            $antes = $query->result();
            $antes = $antes[0]->cant;
        else:
            $antes = 0;
        endif;


        $this->load->helper('path');
        $path = set_realpath("_uploads");
        $file = $path.$filename;

        $this->load->library('gpoint');
        $this->load->library('xlsxreader');

        #Si seleccionaron el nodo
        if(!empty($nodo))
        if(is_file($file)):
            //load our new PHPExcel library
            $array = array($file);
            $this->xlsxreader->init($array);

            $sheets = $this->xlsxreader->getSheetNames();
            $sheet = reset($sheets);

            $data = $this->xlsxreader->getSheetData($sheet);


            $info = "";
            $set = "";
            $search = "";
            for($i=0;$i<count($data);$i++):

                if(empty($data[$i][1]) && !empty($data[$i][0])):
                    $info = strtolower($data[$i][0]);
                    $cond = "";
                    #Vemos si viene sector
                    $temp = explode(";",$info);
                    if(count($temp)==1):
                        $temp = explode(" ",$info);
                        $t1 = strtolower($temp[0]);
                        $search = trim(str_replace($t1,"",$info));
                        $cond .= $this->condicion($t1,$search);
                    else:
                        $temp_ = explode(" ",$temp[0]);
                        $t1 = strtolower($temp_[0]);
                        $search = trim(str_replace($t1,"",$temp[0]));
                        $cond .= $this->condicion($t1,$search);

                        $temp_ = explode(" ",$temp[1]);
                        $t1 = strtolower($temp_[0]);
                        $search = trim(str_replace($t1,"",$temp[1]));
                        $cond .= $this->condicion($t1,$search);
                    endif;

                    if($t1=="contador"):
                        $cond = "";
                    endif;

                else:

                    $this->gpoint->setUTM($data[$i][1],$data[$i][2],"15N");
                    $this->gpoint->convertTMtoLL();

                    $lat_ = $this->gpoint->Lat();
                    $lon_ = $this->gpoint->Long();

                    $set = " set LAT = '".$lat_."',
                             LON = '".$lon_."'";

                    #Quitamos los espacios en blanco
                    $data[$i][0]=trim($data[$i][0]);

                    #Actualizamos las condiciones
                    if($t1!="contador"):
                        $casa = " and (
                                    REPLACE(CASA_No,' ','')='".$data[$i][0]."'
                                    or REPLACE(LOTE,' ','')='".str_replace("L","",$data[$i][0])."'
                                    or trim(CONTADOR)='".$data[$i][0]."'
                                    or trim(APARTAMENTO)='".$data[$i][0]."'
                                    or trim(No_LOCAL)='".$data[$i][0]."'
                                    or trim(OFICINA)='".$data[$i][0]."'
                                  )";
                    else:
                        $casa = " and CONTADOR='".$data[$i][0]."'";
                    endif;

                    if(!($lat_<14 or $lat_>=15 or $lon_<=-91 or $lon_>-90)):
                        $sql = "update direcciones " . $set . "
                            where  NODO='" . $nodo . "' " . $cond . $casa;
                        #echo $sql."<br>";
                    endif;

                    $this->db->query($sql);
                    #echo "<br>".$sql."<br>";


                endif;

            endfor;


            $this->xlsxreader->close();

        endif; #End if is file

        #Contabilizamos cuantos quedan
        $sql = "SELECT count(1) cant
                FROM `direcciones`
                where (lat is NULL
                or lon is NULL)
                and trim(nodo) = '".$nodo."'
                group by nodo";
        $query = $this->db->query($sql);


        if ($query->num_rows() > 0):
            $cantidad = $query->result();
            $cantidad = $cantidad[0]->cant;
        else:
            $cantidad = 0;
        endif;


        #redirect("archivos/cargar_archivo_update/".$nodo."/".$cantidad);
        echo $nodo.",".$antes.",".$cantidad."<br>";
    }

    function condicion($t1,$search){
        $cond = "";
        if($t1=="boulevard" || $t1=="avenida" || $t1=="calzada"):
            #Boulevard
            $cond .= " and (boulevard like '".$search."' ";
            $cond .= " or REPLACE(boulevard,' ','') like '".$search."' ";
            #Avenida
            $cond .= " or avenida like '".$search."'";
            $cond .= " or REPLACE(avenida,' ','') like '".$search."' ";
            #Calzada
            $cond .= " or calzada like '".$search."' ";
            $cond .= " or REPLACE(calzada,' ','') like '".$search."') ";
        endif;

        if($t1=="callejon" || $t1=="pasaje"):
            $cond .= " and (callejon like '".$search."' or pasaje like '".$search."' ";
            $cond .= " or REPLACE(callejon,' ','') like '".$search."' or REPLACE(pasaje,' ','') like '".$search."') ";
        endif;

        if($t1=="colonia" || $t1=="condominio" || $t1=="residencial" || $t1=="urbanizacion" || $t1=="barrio"):
            $cond .= " and (colonia like '".$search."' ";
            $cond .= " or condominio like '".$search."' ";
            $cond .= " or residencial like '".$search."' ";
            $cond .= " or urbanizacion like '".$search."' ";
            $cond .= " or barrio like '".$search."') ";
        endif;

        if($t1=="comercio" || $t1=="mercado" || $t1=="comercial" || $t1=="plaza"):
            $cond .= " and comercio like '".$search."' ";
            $cond .= " and mercado like '".$search."' ";
            $cond .= " and c_comercial like '".$search."' ";
            $cond .= " and plaza like '".$search."' ";
        endif;

        if($t1=="proyecto" || $t1=="asentamiento"):
            $cond .= " and (proyecto like '".$search."' ";
            $cond .= " or asentamiento like '".$search."') ";
        endif;

        if($t1=="sector" || $t1=="lote" || $t1=="manzana"):
            $cond .= " and (sector like '".$search."' ";
            $cond .= " or lote like '".$search."' ";
            $cond .= " or manzana like '".$search."') ";
        endif;

        if($t1=="aldea" || $t1=="canton" || $t1=="caserio"):
            $cond .= " and (aldea like '".$search."' ";
            $cond .= " or canton like '".$search."' ";
            $cond .= " or caserio like '".$search."') ";
        endif;

        if($t1=="ruta" || $t1=="carretera"):
            $cond .= " and (ruta like '".$search."' ";
            $cond .= " or carretera like '".$search."') ";
        endif;

        if($t1=="parcela" || $t1=="finca"):
            $cond .= " and (parcela like '".$search."' ";
            $cond .= " or finca like '".$search."') ";
        endif;


        switch($t1):
            case "poste":
                $cond .= " and poste like '".$search."' ";
                break;
            case "calle":
                $cond .= " and (calle like '".$search."'";
                $cond .= " or REPLACE(calle,' ','') like '".$search."') ";
                break;
            case "kilometro":
                $cond .= " and kilometro like '".$search."' ";
                break;
            case "diagonal":
                $cond .= " and diagonal like '".$search."' ";
                break;
            case "via":
                $cond .= " and via like '".$search."' ";
                break;
            case "num":
                $cond .= " and casa_no like '".$search."' ";
                break;
            case "contador":
                $cond .= " and contador like '".$search."' ";
                break;
            case "zona":
                $cond .= " and zona like '".$search."' ";
                break;
            case "edificio":
                $cond .= " and edificio like '".$search."' ";
                break;
            case "nivel":
                $cond .= " and nivel like '".$search."' ";
                break;
            case "apartamento":
                $cond .= " and apartamento like '".$search."' ";
                break;
            case "institucion":
                $cond .= " and institucion like '".$search."' ";
                break;
            case "local":
                $cond .= " and no_local like '".$search."' ";
                break;
            case "oficina":
                $cond .= " and oficina like '".$search."' ";
                break;
            case "cluster":
                $cond .= " and cluster like '".$search."' ";
                break;
            case "condado":
                $cond .= " and condado like '".$search."' ";
                break;
            case "fase":
                $cond .= " and fase like '".$search."' ";
                break;
            case "tronco":
                $cond .= " and tronco like '".$search."' ";
                break;
            case "departamento":
                $cond .= " and ciudad like '".$search."' ";
                break;
            case "municipio":
                $cond .= " and municipio like '".$search."' ";
                break;
            case "operador":
                $cond .= " and operador like '".$search."' ";
                break;
            case "red":
                $cond .= " and red like '".$search."' ";
                break;
            case "localizacion":
                $cond .= " and localizacion like '".$search."' ";
                break;
            case "catastral":
                $cond .= " and catastral like '".$search."' ";
                break;
            case "granja":
                $cond .= " and granja like '".$search."' ";
                break;
            case "anexo":
                $cond .= " and anexo like '".$search."' ";
                break;
            case "fraccion":
                $cond .= " and fraccion like '".$search."' ";
                break;
            case "interior":
                $cond .= " and interior like '".$search."' ";
                break;
            case "modulo":
                $cond .= " and modulo like '".$search."' ";
                break;
            case "cerro":
                $cond .= " and cerro like '".$search."' ";
                break;
            case "torre":
                $cond .= " and torre like '".$search."' ";
                break;
            case "kiosko":
                $cond .= " and kiosko like '".$search."' ";
                break;
            case "salon":
                $cond .= " and salon like '".$search."' ";
                break;
            case "referencia":
                $cond .= " and referencia like '".$search."' ";
                break;
            case "restaurante":
                $cond .= " and restaurante like '".$search."' ";
                break;
            case "eje":
                $cond .= " and eje like '".$search."' ";
                break;
            case "lotificacion":
                $cond .= " and lotificacion like '".$search."' ";
                break;
            case "seccion":
                $cond .= " and seccion like '".$search."' ";
                break;
        endswitch;

        return $cond;
    }

#**************************************************************************************************
    #Procesamos el archivo de poligonos del kml
    public function process_poligons($tipo=0){

        $this->load->helper('path');
        $path = set_realpath("_kml");
        #$file = $this->input->post("file");
        if($tipo==0):
            $file = "tigostar.kml";
        else:
            $file = "adquirida.kml";
        endif;

        #Cargamos el xml del kml
        $xml = simplexml_load_file($path.$file,null,LIBXML_NOCDATA,null,true);

        $pm  = array();

        function all_tag($xml,&$pm){
            $i=0; $name = "";
            foreach ($xml as $k){
                $tag = $k->getName();
                #echo $k->name."<br><br>";
                if(trim($tag)=="Placemark"):
                    #echo $tag ."<br>";
                    #var_dump($k);
                    #echo "<br><br><br>";
                    array_push($pm,$k);
                endif;
                // recursive
                all_tag($k->children(),$pm);
            }
        }

        all_tag($xml,$pm);

        #Truncamos la base de datos
        $this->db->from('poligono');
        $this->db->truncate();

        $this->db->delete('poligono', array('tipo' => $tipo));

        $result = $xml->xpath('placemark');
        #var_dump($result);

        #echo "<pre>";
        #if($tipo==0):
            #echo "<pre>";
            #var_dump($xml->Document->Pacemark);
            #echo "</pre>";
            foreach($pm as $pm_):
                #echo "************************************************************************************************************************************************************************************************<br><br>";
                #var_dump($item);
                #echo "************************************************************************************************************************************************************************************************<br><br>";
                #foreach($item->Folder->Placemark as $pm):
                    #echo $pm_->name->__toString()."<br>";
                    $array = array(
                        'nombre'=>$pm_->name->__toString(),
                        'descripcion' => $pm_->description->__toString(),
                        'coordenadas' =>str_replace(",0","",str_replace(" ",";",trim($pm_->Polygon->outerBoundaryIs->LinearRing->coordinates->__toString()))),
                        'tipo' => $tipo
                    );
                    #var_dump($array);
                    $this->db->insert('poligono', $array);

                #endforeach;
            endforeach;
        #endif;

        /*exit(0);

        #echo "<pre>";
        if($tipo==0):
            #echo "<pre>";
            #var_dump($xml->Document->Pacemark);
            #echo "</pre>";
            foreach($xml->Document->Document as $item):
                #echo "************************************************************************************************************************************************************************************************<br><br>";
                #var_dump($item);
                #echo "************************************************************************************************************************************************************************************************<br><br>";
                foreach($item->Folder->Placemark as $pm):
                    echo $pm->name->__toString()."<br>";
                    $array = array(
                        'nombre'=>$pm->name->__toString(),
                        'descripcion' => $pm->description->__toString(),
                        'coordenadas' =>str_replace(",0","",str_replace(" ",";",trim($pm->Polygon->outerBoundaryIs->LinearRing->coordinates->__toString()))),
                        'tipo' => $tipo
                    );
                    #var_dump($array);
                    $this->db->insert('poligono', $array);

                endforeach;
            endforeach;

            foreach($xml->Document->Document as $item):
                #echo "************************************************************************************************************************************************************************************************<br><br><pre>";
                #var_dump($item);
                #echo "</pre>************************************************************************************************************************************************************************************************<br><br>";

                foreach($item->Placemark as $pm):
                    $array = array(
                        'nombre'=>$pm->name->__toString(),
                        'descripcion' => $pm->description->__toString(),
                        'coordenadas' =>str_replace(",0","",str_replace(" ",";",trim($pm->Polygon->outerBoundaryIs->LinearRing->coordinates->__toString()))),
                        'tipo' => $tipo
                    );
                    #var_dump($array);
                    #echo "<br><br>";
                    $this->db->insert('poligono', $array);

                endforeach;

            endforeach;
        else:
            #var_dump($xml);
            foreach($xml->Document->Folder as $item):
                foreach($item->Placemark as $pm):
                    if(!empty($pm->Polygon->outerBoundaryIs->LinearRing->coordinates)):
                        $array = array(
                            'nombre'=>$pm->name->__toString(),
                            'descripcion' => $pm->description->__toString(),
                            'coordenadas' =>str_replace(",0","",str_replace(" ",";",trim($pm->Polygon->outerBoundaryIs->LinearRing->coordinates->__toString()))),
                            'tipo' => $tipo
                        );
                        $this->db->insert('poligono', $array);
                    endif;
                endforeach;

                foreach($item->Folder as $fl):
                    foreach($fl->Placemark as $pm):
                         $array = array(
                            'nombre'=>$pm->name->__toString(),
                            'descripcion' => $pm->description->__toString(),
                            'coordenadas' =>str_replace(",0","",str_replace(" ",";",trim($pm->Polygon->outerBoundaryIs->LinearRing->coordinates->__toString()))),
                            'tipo' => $tipo
                        );
                        $this->db->insert('poligono', $array);
                    endforeach;
                endforeach;
            endforeach;
        endif;
        #var_dump($xml->Document->Folder);
        #echo "</pre>";
        */
    }
#**************************************************************************************************
    #Formulario para cargar el kml
    public function kml(){
        $this->load_header(NULL);
        $this->load->view('archivo_kml');
        $this->load_footer();
    }
    #Formulario para cargar el kml
    public function kml_adquirida(){
        $this->load_header(NULL);
        $this->load->view('archivo_kml_adquirida');
        $this->load_footer();
    }
    #Formulario para cargar el archivo de direcciones
    public function archivo_colonias(){
        $this->load_header(NULL);
        $this->load->view('archivo_colonias');
        $this->load_footer();
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
