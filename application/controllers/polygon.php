<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Description: The point-in-polygon algorithm allows you to check if a point is
inside a polygon or outside of it.
Author: Michaël Niessen (2009)
Website: http://AssemblySys.com

If you find this script useful, you can show your
appreciation by getting Michaël a cup of coffee ;)
PayPal: michael.niessen@assemblysys.com

As long as this notice (including author name and details) is included and
UNALTERED, this code is licensed under the GNU General Public License version 3:
http://www.gnu.org/licenses/gpl.html
*/


class Polygon extends MY_Controller {
    var $pointOnVertex = true; // Check if the point sits exactly on one of the vertices?

    function pointLocation() {
    }

    #function pointInPolygon($point="-90.5998706817627,14.512904295409037", $pointOnVertex = true) {
    function pointInPolygon() {

        $this->load->model("file_model",'file');
        $this->file->set_table("poligono");
        $polygons = $this->file->list_rows("*",'tipo=0');

        #$this->pointOnVertex = $pointOnVertex;
        $this->pointOnVertex = true;

        // Transform string coordinates into arrays with x and y values
        #$point = $this->pointStringToCoordinates($point);
        $point = $this->pointStringToCoordinates($this->input->post("lon").",".$this->input->post("lat"));

        if(!empty($polygons))
        foreach($polygons as $p):

            $polygon = explode(";",$p->coordenadas);;
            $nombre  = $p->nombre;
            $descripcion  = $p->descripcion;

            $vertices = array();

            foreach ($polygon as $vertex) {
                $vertices[] = $this->pointStringToCoordinates($vertex);
            }

            // Check if the point sits exactly on a vertex
            if ($this->pointOnVertex == true and $this->pointOnVertex($point, $vertices) == true) {
                echo $nombre.",".$descripcion;
                return;
            }

            // Check if the point is inside the polygon or on the boundary
            $intersections = 0;
            $vertices_count = count($vertices);

            for ($i=1; $i < $vertices_count; $i++) {
                $vertex1 = $vertices[$i-1];
                $vertex2 = $vertices[$i];
                if ($vertex1['y'] == $vertex2['y'] and $vertex1['y'] == $point['y'] and $point['x'] > min($vertex1['x'], $vertex2['x']) and $point['x'] < max($vertex1['x'], $vertex2['x'])) { // Check if point is on an horizontal polygon boundary
                    echo "boundary";
                }
                #echo $point['y']."<br>";
                #echo min($vertex1['y'], $vertex2['y'])."<br>";
                #echo max($vertex1['y'], $vertex2['y'])."<br>";
                if ($point['y'] > min($vertex1['y'], $vertex2['y']) and $point['y'] <= max($vertex1['y'], $vertex2['y']) and $point['x'] <= max($vertex1['x'], $vertex2['x']) and $vertex1['y'] != $vertex2['y']) {
                    $xinters = ($point['y'] - $vertex1['y']) * ($vertex2['x'] - $vertex1['x']) / ($vertex2['y'] - $vertex1['y']) + $vertex1['x'];
                    if ($xinters == $point['x']) { // Check if point is on the polygon boundary (other than horizontal)
                        echo $nombre.",".$descripcion;
                        return;
                    }
                    if ($vertex1['x'] == $vertex2['x'] || $point['x'] <= $xinters) {
                        $intersections++;
                    }
                }
            }
            // If the number of edges we passed through is odd, then it's in the polygon.
            if ($intersections % 2 != 0) {
                echo $nombre.";@".$descripcion;
                return;
            }
        endforeach;


        #Vemos si esta dentro de los poligonos de red adquirida
        $this->file->set_table("poligono");
        $polygons = $this->file->list_rows("*",'tipo=1');

        #$this->pointOnVertex = $pointOnVertex;
        $this->pointOnVertex = true;

        // Transform string coordinates into arrays with x and y values
        #$point = $this->pointStringToCoordinates($point);
        $point = $this->pointStringToCoordinates($this->input->post("lon").",".$this->input->post("lat"));

        if(!empty($polygons))
        foreach($polygons as $p):

            $polygon = explode(";",$p->coordenadas);;
            $nombre  = $p->nombre;
            $descripcion  = $p->descripcion;

            $vertices = array();

            foreach ($polygon as $vertex) {
                $vertices[] = $this->pointStringToCoordinates($vertex);
            }

            // Check if the point sits exactly on a vertex
            if ($this->pointOnVertex == true and $this->pointOnVertex($point, $vertices) == true) {
                echo $nombre.",".$descripcion;
                return;
            }

            // Check if the point is inside the polygon or on the boundary
            $intersections = 0;
            $vertices_count = count($vertices);

            for ($i=1; $i < $vertices_count; $i++) {
                $vertex1 = $vertices[$i-1];
                $vertex2 = $vertices[$i];
                if ($vertex1['y'] == $vertex2['y'] and $vertex1['y'] == $point['y'] and $point['x'] > min($vertex1['x'], $vertex2['x']) and $point['x'] < max($vertex1['x'], $vertex2['x'])) { // Check if point is on an horizontal polygon boundary
                    echo "boundary";
                }
                #echo $point['y']."<br>";
                #echo min($vertex1['y'], $vertex2['y'])."<br>";
                #echo max($vertex1['y'], $vertex2['y'])."<br>";
                if ($point['y'] > min($vertex1['y'], $vertex2['y']) and $point['y'] <= max($vertex1['y'], $vertex2['y']) and $point['x'] <= max($vertex1['x'], $vertex2['x']) and $vertex1['y'] != $vertex2['y']) {
                    $xinters = ($point['y'] - $vertex1['y']) * ($vertex2['x'] - $vertex1['x']) / ($vertex2['y'] - $vertex1['y']) + $vertex1['x'];
                    if ($xinters == $point['x']) { // Check if point is on the polygon boundary (other than horizontal)
                        echo $nombre.",".$descripcion;
                        return;
                    }
                    if ($vertex1['x'] == $vertex2['x'] || $point['x'] <= $xinters) {
                        $intersections++;
                    }
                }
            }
            // If the number of edges we passed through is odd, then it's in the polygon.
            if ($intersections % 2 != 0) {
                echo $nombre.";@".$descripcion;
                return;
            }
        endforeach;


        echo "fuera";
        return;
    }

    function pointOnVertex($point, $vertices) {
        foreach($vertices as $vertex) {
            if ($point == $vertex) {
                return true;
            }
        }

    }

    function pointStringToCoordinates($pointString) {
        $coordinates = explode(",", $pointString);
        return array("x" => $coordinates[0], "y" => $coordinates[1]);
    }

}
?>
