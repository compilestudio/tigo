<script>
$(document).ready(function(){
})
</script>
<div class="container reporte">
    <h1>
        Seleccione los filtros para desplegar las direcciones a eliminar
    </h1>
    <div class="row">
        <form action="<?=site_url("eliminar/direcciones");?>" method="post">
            <div class="col-lg-3 form-group">
                <select name="nodo" id="nodo" class="form-control">
                    <option value="">
                        Nodo
                    </option>
                    <?php foreach($nodos as $nod):?>
                        <option value="<?=$nod->nodo; ?>" <?php if($nod->nodo==$nodo_) echo "selected";?>>
                            <?=$nod->nodo;?>
                        </option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="col-lg-3 form-group">
                <input type="text" class="form-control" name="search" id="search" placeholder="Ingrese su búsqueda" autocomplete="off" value="<?=$search_;?>">
            </div>
            <div class="col-lg-3 form-group">
                <button type="submit" class="btn btn-primary">
                    Buscar
                </button>
            </div>
        </form>
        <?php if($op==1):?>
            <div class="alert alert-success col-lg-5" role="alert" style="margin-left: 15px;">
                Se han eliminado las direcciones correctamente
            </div>
        <?php endif;?>
    </div>
    <form action="<?=site_url("eliminar/masivo");?>" method="post" id="eliminarMasivo">
        <div class="row" style="margin-bottom: 15px;">
            <div class="col-lg-12">
                <button type="button" class="btn btn-danger del_masivo pull-right">
                    Eliminar todas <span class="glyphicon glyphicon-trash"></span>
                </button>
            </div>
        </div>
        <table class="table table-bordered table-condensed pull-left">
            <tr>
                <th>
                    Nodo
                </th>
                <th>
                    Dirección
                </th>
                <th>
                    Latitud
                </th>
                <th>
                    Longitud
                </th>
                <th>
                    &nbsp;
                </th>
            </tr>
            <?php if(!empty($rows))
            foreach($rows as $r):?>
                <tr>
                    <td>
                        <?=$r->NODO;?>
                    </td>
                    <td>
                        <?=$r->DIRECCION;?>
                    </td>
                    <td>
                        <?=$r->LAT;?>
                    </td>
                    <td>
                        <?=$r->LON;?>
                    </td>
                    <td>
                        <button type="button" class="btn btn-danger btn_delete" data-id="<?=$r->id;?>">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button>
                        <input name="id[]" name="id[]" type="hidden" value="<?=$r->id;?>"/>
                    </td>
                </tr>
            <?php endforeach;?>
        </table>
    </form>
</div>
<script>
$(document).ready(function(){
   $('.btn_delete').on('click',function(){
       var obj = $(this).parent().parent();
        if(confirm('Desea eliminar esta direccion?')){
            $.post('<?=site_url("eliminar/uno");?>',{id:$(this).attr("data-id")},function(){
                $(obj).fadeOut();
            });
        }
   });

    $('.del_masivo').on('click',function(){
        if(confirm('Desea eliminar todas estas direcciones?')){
            $('#eliminarMasivo').submit();
        }
    });
});
</script>