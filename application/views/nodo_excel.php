<table border="1">
    <tr>
        <td style="border:none;">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="71" style="border:none;">
            &nbsp;
        </td>
        <td colspan="2">
            Coordenadas
        </td>
    </tr>
    <tr>
        <?php
        $i=0;
        foreach($fields as $f):
            if($i++>0):
        ?>
            <th <?php if($i<5):?>style="background:#25618F;"<?php endif;?>>
                <?=$f;?>
            </th>
        <?php endif;
        endforeach;?>
    </tr>
    <?php foreach($rows as $row):?>
        <tr>
            <?php $i =0;
            foreach($row as $r):
                if($i++>0):
            ?>
                <td>
                    <?=$r;?>
                </td>
            <?php endif;
            endforeach;?>
        </tr>
    <?php endforeach;?>
</table>