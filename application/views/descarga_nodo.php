<script>
$(document).ready(function(){
    $('#nodo').on('change',function(){
        $('#form_nodo').submit();
    });
});
</script>
<div class="container reporte">
    <h1>
        Seleccione el nodo
        <div class="select_nodo">
            <div class="form-group">
                <form action="<?=site_url("reporte/descarga_nodo");?>" method="post" id="form_nodo">
                    <select name="nodo" id="nodo">
                        <option value="-1">
                            Seleccione un nodo
                        </option>
                        <?php foreach($nodos as $nodo):?>
                            <option <?php if($nodo->nodo==$nodo_seleccionado) echo "selected='selected'";?> value="<?=$nodo->nodo;?>">
                                <?=$nodo->nodo;?> (<?=$nodo->cant;?>)
                            </option>
                        <?php endforeach;?>
                    </select>
                    <?php if(!empty($nodo_seleccionado)):?>
                        <a href="<?=site_url("reporte/descarga_excel/".$nodo_seleccionado);?>" type="submit" class="btn btn-primary pull-right">
                            Descargar
                        </a>
                    <?php endif;?>
                </form>
            </div>
        </div>
    </h1>
    <table class="table table-bordered table-condensed pull-left">
        <tr>
            <th>
                Nodo
            </th>
            <th>
                Dirección
            </th>
            <th>
                Lote
            </th>
            <th>
                Manzana
            </th>
            <th>
                Casa #
            </th>
            <th>
                Latitud
            </th>
            <th>
                Longitud
            </th>
        </tr>
        <?php foreach($rows as $row):?>
            <tr>
                <td>
                    <?=$row->NODO;?>
                </td>
                <td>
                    <?=$row->DIRECCION;?>
                </td>
                <td>
                    <?=$row->MANZANA;?>
                </td>
                <td>
                    <?=$row->LOTE;?>
                </td>
                <td>
                    <?=$row->CASA_No;?>
                </td>
                <td>
                    <?=$row->LAT;?>
                </td>
                <td>
                    <?=$row->LON;?>
                </td>
            </tr>
        <?php endforeach;?>
    </table>
</div>