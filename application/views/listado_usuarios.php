<div class="container reporte">
    <h1>
        Usuarios
        <button class="btn btn-primary pull-right" type="button" id="nuevo_usuario">
            Nuevo usuario
        </button>
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped footable" data-paging="true" data-paging-count-format="{CP} de {TP}" data-paging-size="40" data-filtering="true">
            <thead>
                <tr>
                    <th data-breakpoints="null">Usuario</th>
                    <th>Nombre</th>
                    <th>Tipo</th>
                    <th>Departamento</th>
                    <th data-type="html">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($usuarios))
                foreach($usuarios as $u):?>
                    <tr data-expanded="true">
                        <td id="td1">
                            <?=$u->email;?>
                        </td>
                        <td id="td2">
                            <?=$u->nombre;?>
                        </td>
                        <td id="td3">
                            <?=$u->tipo_;?>
                        </td>
                        <td id="td4">
                            <?=$u->departamento;?>
                        </td>
                        <td data-id="<?=$u->id;?>">
                            <input type="hidden" name="usuario" class="usuario" value="<?=$u->email;?>">
                            <input type="hidden" name="nombre" class="nombre" value="<?=$u->nombre;?>">
                            <input type="hidden" name="tipo" class="tipo" value="<?=$u->tipo;?>">
                            <input type="hidden" name="departamento" class="departamento" value="<?=$u->departamento;?>">
                            <input type="hidden" name="id" class="id" value="<?=$u->id;?>">
                            <button type="button" class="btn btn-primary editar_usuario">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-danger delete_user">
                                <i class="glyphicon glyphicon-remove-sign"></i>
                            </button>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="editarUsuario">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">
            Editar Usuario
        </h4>
      </div>
      <div class="modal-body">
        <form id="edit_user">
            <div class="form-group">
                <label for="usuario">
                    Usuario
                </label>
                <input type="text" class="form-control validate[required]" id="usuario_" name="usuario">
            </div>
            <div class="form-group">
                <label for="password">
                    Password
                </label>
                <input type="password" class="form-control validate[funcCall[valPasword]]" id="password" name="password">
            </div>
            <div class="form-group">
                <label for="nombre">
                    Nombre
                </label>
                <input type="text" class="form-control validate[required]" id="nombre_" name="nombre">
            </div>
            <div class="form-group">
                <label for="tipo_">
                    Tipo
                </label>
                <select class="form-control" name="tipo_" id="tipo_">
                    <?php if(!empty($tipos))
                    foreach($tipos as $t):?>
                        <option value="<?=$t->id;?>">
                            <?=$t->tipo;?>
                        </option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="form-group">
                <label for="departamento_">
                    Departamento
                </label>
                <select class="form-control" name="departamento_" id="departamento_">
                    <?php if(!empty($deptos))
                    foreach($deptos as $t):?>
                        <option value="<?=$t->name;?>">
                            <?=$t->name;?>
                        </option>
                    <?php endforeach;?>
                </select>
                <input type="hidden" name="id" id="id_">
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary guardar_edicion">Guardar Cambios</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
var row = null;
var validate_password = 0;
$(document).ready(function(){
    $("#edit_user").validationEngine('attach');
    $('.footable').footable();
    /**
     * Eliminar Usuario
     */
    $('.table').on('click', '.delete_user', function(){
        var obj = $(this).parent().parent();
        var id = $(this).parent().attr("data-id");
        bootbox.confirm("¿Seguro que quiere eliminar este usuario?", function(result) {
            if(result){
                $.post(
                    "<?=site_url('admin/eliminar_usuario');?>",
                    {
                        id:id
                    },
                    function(data){
                        $(obj).fadeOut();
                        bootbox.alert("Usuario eliminado!");
                    }
                );
            }
        });
    });
    /**
     * Editar Usuario
     */
    $('.table').on('click', '.editar_usuario' ,function(){
        $('#edit_user').validationEngine('hideAll');
        var obj = $(this).parent().parent();
        row = obj;
        $("#usuario_").val($(".usuario",$(obj)).val());
        $("#nombre_").val($(".nombre",$(obj)).val());
        $("#id_").val($(".id",$(obj)).val());
        $('#password').val("");
        $('#tipo_ option[value="' + $(".tipo",$(obj)).val() + '"]').prop('selected', true);
        $('#departamento_ option[value="' + $(".departamento",$(obj)).val() + '"]').prop('selected', true);
        validate_password = 0;
        $('#editarUsuario').modal('show');
    });
    /**
     * Nuevo Usuario
     */
    $('#nuevo_usuario').on('click',function(){
        $('#edit_user').validationEngine('hideAll');
        $("#id_,#password,#nombre_,#usuario_").val('');
        $('#tipo_,departamento_').prop('selectedIndex',0);
        validate_password = 1;
        $('#editarUsuario').modal('show');
    });
    /**
     * Guardar edicion de usuario
     */
    $('.guardar_edicion').on('click',function(){
        if($("#edit_user").validationEngine('validate')) {
            $.post(
                "<?=site_url('admin/guardar_edits');?>",
                {
                    usuario: $('#usuario_').val(),
                    nombre: $('#nombre_').val(),
                    password: $('#password').val(),
                    tipo: $('#tipo_').val(),
                    departamento: $('#departamento_').val(),
                    id: $('#id_').val()
                },function(data){
                    if(data==="1"){
                        $("#td1",$(row)).html($('#usuario_').val());
                        $(".usuario",$(row)).val($('#usuario_').val());
                        $("#td2",$(row)).html($('#nombre_').val());
                        $(".nombre",$(row)).val($('#nombre_').val());

                        $("#td3",$(row)).html($('#tipo_ option:selected').text());
                        $(".tipo",$(row)).val($('#tipo_ option:selected').text());                    

                        $("#td4",$(row)).html($('#departamento_').val());
                        $(".departamento",$(row)).val($('#departamento_').val());

                        $('#editarUsuario').modal('hide');
                        bootbox.alert("Usuario guardado con éxito!");
                    }else{
                        bootbox.alert("Este usuario ya existe en la base de datos!");
                    }
                }
            );
        }
    });
});
function valPasword(field, rules, i, options){
    if(validate_password == 0){
        return true;
    }else if (field.val() === "") {
        rules.push('required');
        return "Debe de ingresar una contraseña.";
    }
}
</script>