<div class="container reporte">
    <h1>
        Control de Usuarios
    </h1>
    <h4>
        Filtros
    </h4>
    <div class="row">
        <form action="<?=site_url("reporte/control_usuarios");?>" method="get">
            <div class="col-md-3">
                <label for="">
                    Seleccione rango de fechas
                </label>
                <div class="input-daterange input-group" id="datepicker">
                    <input type="text" class="form-control" name="start" value="<?=$ini;?>" />
                    <span class="input-group-addon">a</span>
                    <input type="text" class="form-control" name="end" value="<?=$fin;?>" />
                </div>
            </div>
            <div class="col-md-2">
                <label for="nombre">
                    Nombre
                </label>
                <div class="form-group">
                    <input type="text" class="form-control" name="nombre" id="nombre" value="<?=$nombre;?>" />
                </div>
            </div>
            <div class="col-md-2">
                <label for="usuario">
                    Usuario
                </label>
                <div class="form-group">
                    <input type="text" class="form-control" name="usuario" id="usuario" value="<?=$usuario;?>" />
                </div>
            </div>
            <div class="col-md-2">
                <label for="perfil">
                    Perfil
                </label>
                <div class="form-group">
                    <select name="perfil" id="perfil" class="form-control">
                        <option value="-1">
                            Seleccione Perfil
                        </option>
                        <?php if(!empty($tipo_usuario))
                            foreach($tipo_usuario as $d):?>
                                <option value="<?=$d->id;?>" <?php if($perfil == $d->id) echo "selected";?>>
                                    <?=$d->tipo;?>
                                </option>
                            <?php endforeach;?>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <label for="departamento">
                    Departamento
                </label>
                <div class="form-group">
                    <select name="departamento" id="departamento" class="form-control">
                        <option value="-1">
                            Seleccione Departamento
                        </option>
                        <?php if(!empty($departamentos))
                            foreach($departamentos as $d):?>
                                <option <?php if($departamento == $d->name) echo "selected";?>>
                                    <?=$d->name;?>
                                </option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
            <!--
            <div class="col-md-3">
                <label for="unidad">
                    Unidad de Negocio
                </label>
                <div class="form-group">
                    <select name="unidad" id="unidad" class="form-control">
                        <option value="-1">
                            Seleccione Unidad de Negocio
                        </option>
                        <?php if(!empty($unidad_negocio))
                            foreach($unidad_negocio as $u):?>
                                <option value="<?=$u->id;?>" <?php if($unidad == $u->id) echo "selected";?>>
                                    <?=$u->unidad;?>
                                </option>
                            <?php endforeach;?>
                    </select>
                </div>
            </div>
            -->
            <div class="col-md-4">
                <label for="activo">
                    Activo/Inactivo
                </label>
                <select id="activo" name="activo" class="form-control">
                    <option value="1" <?php if($activo==1) echo "selected";?>$>Todos</option>
                    <option value="2" <?php if($activo==2) echo "selected";?>>Activo</option>
                    <option value="3" <?php if($activo==3) echo "selected";?>>Inactivo</option>
                </select>
            </div>
            <div class="col-md-4">
                <label for="tipo_reporte">
                    Tipo Reporte
                </label>
                <select id="tipo_reporte" name="tipo_reporte" class="form-control">
                    <option value="1" <?php if($tipo_reporte==1) echo "selected";?>>Detalle</option>
                    <option value="2" <?php if($tipo_reporte==2) echo "selected";?>>Resumen</option>
                </select>
            </div>
            <div class="col-md-4" style="padding-top:25px;">
                <button class="btn btn-success pull-right" type="submit" id="btn_filtrar" data-val="0">
                    Buscar
                </button>
                <a href="<?=site_url('reporte/control_usuarios');?>" class="btn btn-info pull-right" style="margin-right:10px;">
                    Limpiar
                </a>
                <button class="btn btn-info pull-right" type="submit" style="margin-right:10px;" id="btn_descargar" data-val="1">
                    Descargar
                </button>
                <input type="hidden" name="descargar" id="descargar" value="0">
            </div>
            <!--
            <div class="col-md-8">
                Ordenar por
                <label class="radio-inline">
                    <input type="radio" name="order_by" id="inlineRadio1" value="1" <?php if(empty($order_by) || $order_by==1) echo "checked" ?>>
                    Nombre
                </label>
                <label class="radio-inline">
                    <input type="radio" name="order_by" id="inlineRadio2" value="2" <?php if($order_by==2) echo "checked" ?>>
                    Usuario
                </label>
                <label class="radio-inline">
                    <input type="radio" name="order_by" id="inlineRadio3" value="3" <?php if($order_by==3) echo "checked" ?>>
                    Perfil
                </label>
                <label class="radio-inline">
                    <input type="radio" name="order_by" id="inlineRadio4" value="4" <?php if($order_by==4) echo "checked" ?>>
                    Departamento
                </label>
                <label class="radio-inline">
                    <input type="radio" name="order_by" id="inlineRadio5" value="5" <?php if($order_by==5) echo "checked" ?>>
                    Unidad de Negocio
                </label>
                <label class="radio-inline">
                    <input type="radio" name="order_by" id="inlineRadio6" value="6" <?php if($order_by==6) echo "checked" ?>>
                    Fecha de Alta
                </label>
            </div>
            <div class="col-md-4">
                <label class="radio-inline">
                    <input type="radio" name="direccion" id="direccion1" value="1" <?php if(empty($direccion) || $direccion==1) echo "checked" ?>>
                    Asc
                </label>
                <label class="radio-inline">
                    <input type="radio" name="direccion" id="direccion2" value="2" <?php if($direccion==2) echo "checked" ?>>
                    Desc
                </label>
            </div>
            -->
        </form>
    </div>
    <div class="row" style="margin-top:15px;">
        <div class="col-md-12">
            <table class="table table-bordered table-condensed pull-left" data-paging="true" data-paging-count-format="{CP} de {TP}" data-paging-size="40" data-filtering="false" data-sorting="true">
                <thead>
                    <tr>
                        <?php if($tipo_reporte==1):?>
                            <th>
                                Fecha
                            </th>
                        <?php endif;?>
                        <th>
                            Nombre
                        </th>
                        <th>
                            Usuario
                        </th>
                        <th>
                            Total Minutos
                        </th>
                        <th>
                            Visitas
                        </th>
                        <th data-breakpoints="null">
                            Promedio Diario Minutos
                        </th>
                        <?php if($tipo_reporte==1):?>
                            <th data-type="html" data-breakpoints="all" data-title="Detalle ingresos">
                            </th>
                        <?php endif;?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($rows as $row):?>
                        <tr>
                            <?php if($tipo_reporte==1):?>
                                <td>
                                    <?=$row['fecha_bitacora'];?>
                                </td>
                            <?php endif;?>
                            <td>
                                <?=$row['nombre'];?>
                            </td>
                            <td>
                                <?=$row['email'];?>
                            </td>
                            <td>
                                <?=$row['total_minutos'];?>
                            </td>
                            <td>
                                <?=$row['visitas'];?>
                            </td>
                            <td>
                                <?=$row['promedio_minutos'];?>
                            </td>
                            <?php if($tipo_reporte==1):?>
                                <td>
                                    <?=str_replace("#@#","<br>",$row['horas']);?>
                                </td>
                            <?php endif;?>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('.table').footable();
    $('.input-daterange').datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        orientation: "top",
        autoclose: true
    });
    $('#btn_descargar,#btn_filtrar').on('click',function(){
        $('#descargar').val($(this).attr("data-val"));
    });
});
</script>