<script>
$(document).ready(function(){
    var a = 0;
    //Armamos el uploadify
    $('#archivos').each(function(){
        var parent = $(this).parent();
        $(this).uploadify({
            'buttonImage' : '<?=base_url();?>_img/upload_images2.jpg',
            'width'    : 167,
            'height'   : 39,
            'swf'      : '<?=base_url();?>uploadify.swf',
            'uploader' : '<?=site_url("archivos/upload_file");?>' ,
            'onUploadStart' : function(file) {
                $('.conf').fadeOut();
                $('.loading img').fadeIn();
                a++;
            },
            'onUploadSuccess' : function(file, data, response) {
                //console.log('The file ' + file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
                $.post('<?=site_url("archivos/process_dxf");?>',{file:data},function(){
                    a--;
                    if(a<=0){
                        $('.loading img').fadeOut();
                        $('.conf').fadeIn();
                    }
                });
            }
        });
    });
})
</script>
<div class="container reporte">
    <h1>
        Seleccione los archivos a cargar (.dxf)
    </h1>
    <div class="row-lg-12 archivos_cont">
        <input type="file" name="archivos" id="archivos"/>
        <div class="pull-right loading">
            <img src="<?=base_url();?>_img/loading.gif" alt="loading" class="img-responsive"/>
        </div>
        <div class="alert alert-success col-lg-6 conf" role="alert">
            <span class="glyphicon glyphicon-ok"></span>
            Archivos cargados exitosamente
        </div>
    </div>
</div>