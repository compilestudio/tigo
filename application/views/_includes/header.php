
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>
        Geopointer
    </title>
    <?php echo link_tag('_css/style.css');?>
    <?php echo link_tag('_css/footable.bootstrap.min.css');?>
    <?php echo link_tag('_css/leaflet.css');?>
    <?php echo link_tag('_css/validationEngine.jquery.css');?>
    <script language="JavaScript" src="<?=base_url();?>_js/jquery.min.js"></script>
    <!--Leaflet-->
    <script language="JavaScript" src="<?=base_url();?>_js/leaflet.js"></script>
    <script language="javascript" src="<?=base_url();?>_js/leaflet-providers.js"></script>

    <!--Bootstrap-->
    <script language="JavaScript" src="<?=base_url();?>_js/collapse.js"></script>
    <script language="JavaScript" src="<?=base_url();?>_js/modal.js"></script>
    <script language="JavaScript" src="<?=base_url();?>_js/dropdown.js"></script>
    <script language="JavaScript" src="<?=base_url();?>_js/bootbox.min.js"></script>
    <script language="JavaScript" src="<?=base_url();?>_js/bootbox.min.js"></script>
    <script language="JavaScript"  src="<?=base_url();?>_js/alert.js"></script>
    <script language="JavaScript"  src="<?=base_url();?>_js/footable.min.js"></script>

    <!-- Para subir archivos-->
    <script language="JavaScript" src="<?=base_url();?>_js/jquery.uploadify.min.js"></script>

    <!-- Para validar formulario-->
    <script language="JavaScript" src="<?=base_url();?>_js/jquery.validationEngine.js"></script>
    <script language="JavaScript" src="<?=base_url();?>_js/jquery.validationEngine-es.js"></script>

    <!--Estos scripts se usan para desplegar google-->
    <script src="https://maps.google.com/maps/api/js?v=3.2&sensor=false"></script>
    <script language="javascript" src="<?=base_url();?>_js/Google.js"></script>
    <!--
    <script language="javascript" src="<?=base_url();?>_js/leaflet-google.js"></script>
    -->

    <!--Libreria para cargar KML-->
    <script language="javascript" src="<?=base_url();?>_js/KML.js"></script>

    <!-- Autocomplete-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <!-- Date Picker-->
    <script src="<?=base_url();?>_js/bootstrap-datepicker.js"></script>
    <script src="<?=base_url();?>_js/bootstrap-datepicker.es.js"></script>


</head>
<body>
<?php if($this->router->class!="landing"):
    $tipo = $this->session->userdata('tigo_tipo');
?>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav_collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?=site_url("mapa");?>" style="width: 215px; padding-top: 1px;">
                <img src="<?=base_url();?>_img/logo.png" class="img-responsive">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="nav_collapse" style="margin-left:75px;">
            <ul class="nav navbar-nav">
                <!--
                <li <?php if($this->router->class=="mapa") echo 'class="active"';?>>
                    <a href="<?=site_url('mapa')?>">
                        Mapa
                    </a>
                </li>
                -->
                <?php if($tipo==1):?>
                    <li <?php if($this->uri->segment(1)=="archivos") echo 'class="active"';?>>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Archivos
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="<?=site_url('archivos/direcciones');?>">
                                    Archivo de Direcciones
                                </a>
                            </li>
                            <li>
                                <a href="<?=site_url("archivos/kml");?>">
                                    Cargar KML red Tigo Star
                                </a>
                            </li>
                            <li>
                                <a href="<?=site_url("archivos/kml_adquirida");?>">
                                    Cargar KML red Adquirida
                                </a>
                            </li>
                            <li>
                                <a href="<?=site_url("archivos/archivo_colonias");?>">
                                    Cargar Colonias
                                </a>
                            </li>
                            <!--
                            <li class="divider"></li>
                            <li>
                                <a href="<?=site_url('archivos/cargar_archivo_update');?>">
                                    Actualización de coordenadas
                                </a>
                            </li>
                            <li>
                                <a href="<?=site_url("update");?>">
                                    Direcciones sin Coordenadas
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?=site_url('archivos/autocad');?>">
                                    Archivos Autocad (dxf)
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Archivos Postes
                                </a>
                            </li>
                        -->
                        </ul>
                    </li>
                <?php endif;?>
                <?php if($tipo==1 || $tipo==2):?>
                    <li <?php if($this->uri->segment(1)=="reporte") echo 'class="active"';?>>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Reportes
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li <?php if($this->uri->segment(2)=="descarga_nodo") echo 'class="active"';?>>
                                <a href="<?=site_url("reporte/descarga_nodo");?>">
                                    Descargar nodos con coordenadas
                                </a>
                            </li>
                            <li <?php if($this->uri->segment(2)=="registro_clientes") echo 'class="active"';?>>
                                <a href="<?=site_url("reporte/registro_clientes");?>">
                                    Registro de clientes
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li <?php if($this->uri->segment(2)=="nodos-cargados") echo 'class="active"';?>>
                                <a href="<?=site_url("reporte/nodos_cargados");?>">
                                    Nodos Cargados
                                </a>
                            </li>
                            <li <?php if($this->uri->segment(2)=="control_usuarios") echo 'class="active"';?>>
                                <a href="<?=site_url("reporte/control_usuarios");?>">
                                    Control de Usuarios
                                </a>
                            </li>
                            <!--
                            <li>
                                <a href="<?=site_url("update");?>">
                                    Direcciones sin Coordenadas
                                </a>
                            </li>
                            <li>
                                <a href="<?=site_url("reporte/coordenadas_cero");?>">
                                    Direcciones con coordenadas cero
                                </a>
                            </li>
                            -->
                        </ul>
                    </li>
                <?php endif;?>
                <?php if($tipo==1 || $tipo==4):?>
                    <li <?php if($this->uri->segment(1)=="admin") echo 'class="active"';?>>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Administración
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="<?=site_url("admin/usuario");?>">
                                    Usuarios
                                </a>
                            </li>
                            <?php if($tipo==1):?>
                                <li>
                                    <a href="<?=site_url("admin/ips");?>">
                                        Ips autorizadas
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=site_url("eliminar/direcciones");?>">
                                        Eliminar Direcciones
                                    </a>
                                </li>                                
                            <?php endif;?>
                        </ul>
                    </li>
                <?php endif;?>
                <li <?php if($this->uri->segment(1)=="herramienta") echo 'class="active"';?>>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Ayuda
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <?php if($tipo==3):?>
                            <li>
                                <a href="<?=base_url();?>manual/manualsc.pdf" target="_blank">
                                    Manual Usuario
                                </a>
                            </li>
                        <?php elseif($tipo==4):?>
                            <li>
                                <a href="<?=base_url();?>manual/manualit.pdf" target="_blank">
                                    Manual Usuario
                                </a>
                            </li>
                        <?php else:?>
                            <li>
                                <a href="<?=base_url();?>manual/manualc.pdf" target="_blank">
                                    Manual Usuario
                                </a>
                            </li>
                        <?php endif;?>
                        <li>
                            <a href="<?=base_url();?>_uploads/colonias.xlsx">
                                Descargar archivo de colonias
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <?=$this->session->userdata('user_name');?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#CambioContrasena">
                                <span class="glyphicon glyphicon-lock"></span>
                                Cambiar Contraseña
                            </a>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#CambioContrasena" data-backdrop="static" data-keyboard="false" id="forced" style="display:none;">
                                <span class="glyphicon glyphicon-lock"></span>
                                Cambiar Contraseña
                            </a>
                        </li>
                        <li>
                            <a href="<?=site_url('session/end')?>">
                                <span class="glyphicon glyphicon-log-out"></span>
                                Logout
                            </a>

                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<?php endif;?>
<!-- Modal para capturar la información del cliente -->
<div class="modal fade" id="CambioContrasena" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <?php if($forced!=1):?>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php endif;?>
                <h4 class="modal-title" id="myModalLabel">
                    Cambiar la contraseña
                    <?php if($forced==1):?>
                        <strong style="color:#446DAC;">
                            OBLIGATORIO
                        </strong>
                    <?php endif;?>
                </h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="cambia_pass">
                    <div class="form-group">
                        <label for="contrasena">
                            Contraseña Nueva
                        </label>
                        <input type="password" class="form-control validate[required]" id="contrasena">
                    </div>
                    <div class="form-group">
                        <label for="confirmar">
                            Confirmar Contraseña
                        </label>
                        <input type="password" class="form-control validate[required,equals[contrasena]]" id="confirmar">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <?php if($forced!=1):?>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <?php endif;?>
                <button type="button" class="btn btn-primary guardar_pass">Guardar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exito" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Contraseña actualizada con éxito
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $("#cambia_pass").validationEngine('attach',{promptPosition:"topLeft"});

    $('.guardar_pass').on('click',function(){
        if($("#cambia_pass").validationEngine('validate')) {

            $.post('<?=site_url("mapa/validate_user");?>',{pass:$('#contrasena').val()},function(data){
                if(data){
                    $('#contrasena').validationEngine('showPrompt', data, 'error','topLeft')
                }else{
                    $.post('<?=site_url("mapa/change_pass");?>',
                        {
                            contrasena:$('#contrasena').val(),
                            conf:$('#confirmar').val()
                        },
                        function(){
                            $('#CambioContrasena').modal('hide');
                            $('#contrasena,#confirmar').val('');
                            $('#exito').modal('show');
                        }
                    );
                }
            });
        }
    });
    <?php if($forced==1):?>
         $('#forced').click();
    <?php endif;?>
});
</script>