<style>
#report-error {
    color: red;
    display: block;
    font-family: arial;
    font-weight: bold;
    margin-left: 30px;
}
#crudForm{
    margin-bottom: 50px;
}
.report-div{
    margin-left:30px;
}
#force_field_box{
    opacity:0;
}
</style>
<div class="container reporte" style="padding: 0px 50px;">
    <?php
    foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>

        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>


    <div class="central">
        <?php echo $output; ?>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('form').on('submit',function(){
        if($('input[name="password"]').val()!=$('input[name="previous"]').val()){
            $('input[name="force"]').val(1);
        }
    });
});
</script>