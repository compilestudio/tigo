<script>
var a = 1;
$(document).ready(function(){
    //Armamos el uploadify
    $('#archivos').each(function(){
        var parent = $(this).parent();
        $(this).uploadify({
            'buttonImage' : '<?=base_url();?>_img/upload_images2.jpg',
            'width'    : 167,
            'height'   : 39,
            'fileTypeExts' : '*.xlsx',
            'swf'      : '<?=base_url();?>uploadify.swf',
            'uploader' : '<?=site_url("archivos/upload_file");?>' ,
            'onUploadSuccess' : function(file, data, response) {
                //$('#filename').val(data);
                //$('#archive_name').html(data);
                $.post('<?=site_url('archivos/guardar_update');?>',
                    {
                        filename:data
                    },
                    function(data1){
                        $('#append').append(a+'.- '+data1);
                        a++;
                    }
                );


            }
        });
    });
    $("#actualizar").validationEngine({promptPosition:"topLeft"});
});
</script>
<div class="container reporte">
    <h1>
        Actualización de coordenadas (.xlsx)<br>
        Los archivos deben de tener el nombre del nodo antes de cualquier cosa separado por un espacio en blanco
    </h1>

    <form id="actualizar" action="" method="post">
        <div class="col-lg-4" style="display: none;">
            <label for="nodo">
                Seleccione el nodo a actualizar
            </label>
            <select name="nodo" id="nodo" class="form-control validate[required]">
                <option value="">
                    Seleccione un nodo
                </option>
                <?php foreach($nodos as $nodo):?>
                    <option value="<?=$nodo->nodo;?>">
                        <?=$nodo->nodo;?> (<?=$nodo->cant;?>)
                    </option>
                <?php endforeach;?>
            </select>
        </div>
        <div class="col-lg-2 archivos_cont">
            <label for="">
                Seleccione un archivo
            </label>
            <input type="file" name="archivos" id="archivos"/>
            <div class="pull-right loading">
                <img src="<?=base_url();?>_img/loading.gif" alt="loading" class="img-responsive"/>
            </div>
            <div class="alert alert-success col-lg-6 conf" role="alert">
                <span class="glyphicon glyphicon-ok"></span>
                Archivos cargados exitosamente
            </div>
            <div id="archive_name">

            </div>
            <input type="hidden" name="filename" id="filename"/>
        </div>
        <div class="col-lg-4" style="padding-top:26px;display:none;">
            <button type="submit" class="btn btn-primary">
                Actualizar
            </button>
        </div>
    </form>
    <?php  if(!empty($nodo_)):?>
        <div class="row">
            <div class="col-lg-8">
                <div class="alert alert-success" role="alert">
                    Se ha cargado el nodo
                    <strong>
                        ;<?=$nodo_;?>
                    </strong>
                    ;<?=$cant;?>
                </div>
            </div>
        </div>
    <?php endif;?>
    <div id="append" style="float:left;width:100%;"></div>
</div>