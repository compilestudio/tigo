<?php $tipo = $this->session->userdata('tigo_tipo');?>
<script language="javascript">
var tigostar,adquirida,map1,map2,map3,map4,ad3,ban;
ban =0;
var map,bounds,marker;
var myIcon = L.icon({
    iconUrl: '<?=base_url();?>_img/marker.png',
    iconSize: [40, 40]
});
var myIcon2 = L.icon({
    iconUrl: '<?=base_url();?>_img/marker2.png',
    iconSize: [40, 40]
});
var myIcon3 = L.icon({
    iconUrl: '<?=base_url();?>_img/marker3.png',
    iconSize: [40, 40]
});
var myIcon4 = L.icon({
    iconUrl: '<?=base_url();?>_img/marker4.png',
    iconSize: [40, 40]
});
var myIcon5 = L.icon({
    iconUrl: '<?=base_url();?>_img/marker5.png',
    iconSize: [40, 40]
});
var myIcon6 = L.icon({
    iconUrl: '<?=base_url();?>_img/marker6.png',
    iconSize: [40, 40]
});
$(document).ready(function(){

    $('#departamento').on('change',function(){
        $.post('<?=site_url("mapa/get_municipios");?>',{dep:$(this).val()},function(data){
            $('#municipio').html(data);
        });
    });
    $('#municipio').on('change',function(){
        $.post('<?=site_url("mapa/get_zonas");?>',
            {
                dep:$("#departamento").val(),
                mun:$(this).val()
            },
            function(data){
                $('#zona').html(data);
            }
        );
    });
    map = L.map('map');
    /*
    map.on('load', function(e) {
        setTimeout(function(){
            $('.leaflet-control-layers-overlays .leaflet-control-layers-selector').on('mousedown',function(event){
                $('.leaflet-control-layers-overlays .leaflet-control-layers-selector').not(this).click();
            });
        },3000);

    });
    */
    map.setView([14.502578,-90.609548], 13);
    map.on('click', function(e) {
        $('#latitud,#latitud_levanta').val(e.latlng.lat);
        $('#longitud,#longitud_levanta').val(e.latlng.lng);

        if (typeof marker != 'undefined') {
            map.removeLayer(marker);
        }

        var bounds = [$('#latitud').val(),$('#longitud').val()];
        marker = L.marker(bounds,{icon: myIcon5}).addTo(map);
    });
    //L.tileLayer.provider('Stamen.Watercolor').addTo(map);
    //map1 = L.tileLayer.provider('OpenStreetMap.Mapnik').addTo(map);
    map1 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    map1.addTo(map);

    map2 = L.tileLayer('https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    });

    map3 = L.tileLayer('https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    });

    map4 = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    });


    // map2 = new L.Google('ROADMAP');
    // map3 = new L.Google('SATELLITE');
    // map4 = new L.Google('HYBRID');

    //KML layers
    adquirida = new L.KML("<?=base_url();?>_kml/adquirida.kml?<?=date('dmYGi');?>", {async: true});
    adquirida.on("loaded", function(e) {
        map.fitBounds(e.target.getBounds());
    });
    tigostar = new L.KML("<?=base_url();?>_kml/tigostar.kml?<?=date('dmYGi');?>", {async: true}).addTo(map);
    tigostar.on("loaded", function(e) {
        map.fitBounds(e.target.getBounds());
    });
    ad3 = new L.KML("<?=base_url();?>_kml/capa-grande.kml?v=0.1;?>", {async: true});
    ad3.on("loaded", function(e) {
        map.fitBounds(e.target.getBounds());
    });

    $('#map').on('click','.leaflet-control-layers-selector:eq(5)',function(){
        if(ban==0){
            adquirida.addTo(map);
            ban = 1;
        }else{
            map.removeLayer(adquirida);
            ban =0;
        }
    });

    var tipos_mapa = {
        "Open Street Map": map1,
        "Google": map2,
        "Google Satelital": map3,
        "Google Híbrido": map4
    };

    var kmls = {
        "Tigo Star": tigostar,
        "Adquirida": ad3
    };


    L.control.layers(tipos_mapa,kmls).addTo(map);
    map.attributionControl.setPrefix('');


    //El reset del boton
    $('.reset').on('click',reset);
    $('.buscar_dir').on('click',buscar_dir);
    autocompletar();

    $('.guardar_info').on('click',function(){
        if($("#cliente").validationEngine('validate')) {
            $.post('<?=site_url("mapa/salvar_usuario");?>',
                {
                    nombre: $('#nombre_levanta').val(),
                    direccion: $('#direccion_levanta').val(),
                    email: $('#email_levanta').val(),
                    telefono: $('#telefono_levanta').val(),
                    longitud: $('#longitud_levanta').val(),
                    latitud: $('#latitud_levanta').val(),
                    comentarios: $('#comentarios').val()
                },
                function (data) {
                    bootbox.alert("La información del cliente ha sido almacenada");
                    $('#nombre_levanta,#direccion_levanta,#email_levanta,#telefono_levanta,#longitud_levanta,#latitud_levanta,#comentarios').val('');
                    $('#myModal').modal('hide');
                }
            );
        }
    });

    $(window).on('resize',function(){
        //console.log($(window).height());
        $('#map').height($(window).height()-122);
    });
    $('#map').height($(window).height()-122);
    map._onResize();

    //Función cuando le dan enter en cualquier input
    $('#txt,#poste,#latitud,#longitud').keydown(function (e){
        if(e.keyCode == 13){
            buscar_dir();
        }
    });

    //Funcion del validation
    $("#cliente").validationEngine('attach');
    $("#search").validationEngine('attach', {promptPosition : "bottomLeft"});

    $('.boton_capturar').on('click',function(){
        $('#cliente').validationEngine('hideAll');
    });
    //Cuando seleccionen un nodo
    $('#nodo').on('change',function(){
        //search($(this).val());
    });
});
/*
 * Function to reset the form
 * */
function reset(){
    $('.no_cobertura,.poste').fadeOut();
    $(':input').val('');
    map.panTo([14.502578,-90.609548]);
    map.setZoom(9);
    if (typeof marker != 'undefined') {
        map.removeLayer(marker);
    }
}
 /*
* Function to get the coordinates from the search
* */
function search(id){
    //reset();
    $.post('<?=site_url("mapa/search");?>',{id:id},
        function(data){
            var bounds = [data[0].lat,data[0].lon];
            if (typeof marker != 'undefined') {
                map.removeLayer(marker);
            }
            if(data[0].observaciones) {
                if(data[0].observaciones.indexOf("PENDIENTE")>=0){
                    marker = L.marker(bounds,{icon: myIcon2}).addTo(map);
                    marker.bindPopup("<strong>Nodo</strong>: "+data[0].nodo+"<br>"+data[0].observaciones).openPopup();
                }else{
                    marker = L.marker(bounds,{icon: myIcon}).addTo(map);
                    marker.bindPopup("<strong>Nodo</strong>: "+data[0].nodo).openPopup();
                }
            }else{
                marker = L.marker(bounds,{icon: myIcon}).addTo(map);
                marker.bindPopup("<strong>Nodo</strong>: "+data[0].nodo).openPopup();
            }
            $('.loading img').fadeOut();
            map.setZoom(16);
            map.panTo(bounds);

        }
    );
}
/*
 * Function to search a dir
 * */
function buscar_dir(){
    if($('#nodo').val()!=''){
      search($('#nodo').val());
    }else{
        if($("#search").validationEngine('validate')) {
            $('.no_cobertura,.poste').fadeOut();
            $('.loading img').fadeIn();
            if (typeof marker != 'undefined') {
                map.removeLayer(marker);
            }
            if ($('#direccion').val() != '') {
                search();
            } else if ($('#poste').val() != '') {

                $.post('<?=site_url("mapa/poste");?>', {poste: $('#poste').val()}, function (data) {
                    if (data) {
                        var bounds = [data[0].lat, data[0].lon];
                        $.post('<?=site_url("polygon/pointInPolygon/");?>',
                            {
                                lat: data[0].lat,
                                lon: data[0].lon
                            },
                            function(data){
                                $('.loading img').fadeOut();
                                if(data=="fuera") {
                                    marker = L.marker(bounds, {icon: myIcon6}).addTo(map);
                                    marker.bindPopup("Fuera del área de cobertura de la red Tigo Star").openPopup();
                                }else{
                                    marker = L.marker(bounds, {icon: myIcon4}).addTo(map);
                                    var info = data.split(";@");
                                    marker.bindPopup("<strong>"+info[0]+"</strong><br>"+info[1]).openPopup();
                                }
                                map.setZoom(16);
                                map.panTo(bounds);
                            }
                        );
                    } else {
                        $('.loading img').fadeOut();
                        $('.poste').fadeIn();
                    }
                });
            } else if ($('#latitud').val() != '' && $('#longitud').val() != '') {
                var bounds = [$('#latitud').val(), $('#longitud').val()];
                $.post('<?=site_url("polygon/pointInPolygon/");?>',
                    {
                        lat: $('#latitud').val(),
                        lon: $('#longitud').val()
                    },
                    function(data){
                        $('.loading img').fadeOut();
                        if(data=="fuera") {
                            marker = L.marker(bounds, {icon: myIcon6}).addTo(map);
                            marker.bindPopup("Fuera del área de cobertura de la red Tigo Star").openPopup();
                        }else{
                            marker = L.marker(bounds, {icon: myIcon3}).addTo(map);
                            var info = data.split(";@");
                            marker.bindPopup("<strong>"+info[0]+"</strong><br>"+info[1]).openPopup();
                        }
                        map.setZoom(16);
                        map.panTo(bounds);
                    }
                );
            }
        }
    }
}

function autocompletar(){
    $( "#direccion" ).autocomplete({
        //source: "<?=site_url('mapa/autocompletar');?>",
        source: function(request, response) {
            $.getJSON("<?=site_url('mapa/autocompletar');?>",
                {
                    departamento: $('#departamento').val(),
                    municipio: $('#municipio').val(),
                    zona: $('#zona').val(),
                    term: $('#direccion').val()
                }
                ,response
            );
        },
        minLength: 6,
        select: function( event, ui ) {
            $('.loading img').fadeIn();
            search(ui.item.id);
            $('#direccion').blur();
            $('.no_cobertura,.poste').hide();
        },
        search: function( event, ui ) {
            $('.no_cobertura,.poste').fadeOut();
            $('.loading img').fadeIn();
        },
        response: function( event, ui ) {
            $('.loading img').fadeOut();
            if(ui.content.length==0 || !ui.content){
                //$('.cobertura').fadeOut();
                $('.no_cobertura').fadeIn();
                if (typeof marker != 'undefined') {
                    map.removeLayer(marker);
                }
            }else{
                //$('.cobertura').fadeIn();
            }
        }
    });
}
 </script>
<h1 class="alert alert-danger no_cobertura" role="alert">
    <span class="glyphicon glyphicon-exclamation-sign"></span>
    No existe cobertura
</h1>
<h1 class="alert alert-danger poste" role="alert">
    <span class="glyphicon glyphicon-thumbs-down"></span>
    No existe el poste
</h1>
<div class="container-fluid busqueda">
    <form action="" method="post" id="search">
        <div class="row">
            <div class="form-group col-lg-1 col-md-3 col-sm-4 col-xs-4">
                <select name="nodo" id="nodo" class="form-control">
                    <option value="">
                        Nodo
                    </option>
                    <?php foreach($nodos as $nod):?>
                        <option value="<?=$nod->nodo; ?>">
                            <?=$nod->nodo;?>
                        </option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="form-group col-lg-1 col-md-3 col-sm-4 col-xs-4">
                <select name="departamento" id="departamento" class="form-control">
                    <option value="-1">Departamento</option>
                    <?php if(!empty($deptos))
                        foreach($deptos as $d):?>
                            <option>
                                <?=$d->depto;?>
                            </option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="form-group col-lg-1 col-md-3 col-sm-4 col-xs-4">
                <select name="municipio" id="municipio" class="form-control">
                    <option value="-1">Municipio</option>
                </select>
            </div>
            <div class="form-group col-lg-1 col-md-3 col-sm-4 col-xs-4">
                <select name="zona" id="zona" class="form-control">
                    <option value="-1">Zona</option>
                </select>
            </div>
            <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-6">
                <input type="direccion" class="form-control" id="direccion" placeholder="Ingrese la dirección">
            </div>
            <div class="form-group col-lg-1 col-md-2 col-sm-5 col-xs-6">
                <input type="poste" class="form-control validate[custom[integer]]" id="poste" placeholder="Número de poste">
            </div>
            <div class="form-group col-lg-1 col-md-1 col-sm-2 col-xs-2">
                <input type="latitud" class="form-control validate[custom[number]] coord" id="latitud" placeholder="Latitud">
            </div>
            <div class="form-group col-lg-1 col-md-1 col-sm-2 col-xs-2">
                <input type="longitud" class="form-control validate[custom[number]] coord" id="longitud" placeholder="Longitud">
            </div>
            <button class="btn btn-info pull-left reset" type="button">
                <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
            </button>
            <button class="btn btn-primary buscar_dir" type="button">
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
            </button>
            <div class="loading">
                <img src="<?=base_url();?>_img/loading.gif" alt="Loading" class="img-responsive"/>
            </div>
            <?php //if($tipo==1):?>
                <div class="boton_capturar">
                    <button class="btn btn-warning pull-left" type="button" data-toggle="modal" data-target="#myModal">
                        Capturar datos
                    </button>
                </div>
            <?php //endif;?>
        </div>
    </form>
</div>
<div id="map"></div>

<!-- Modal para capturar la información del cliente -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    Datos del Cliente
                </h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="cliente">
                    <div class="form-group">
                        <label for="nombre_levanta">
                            Nombre Completo
                        </label>
                        <input type="text" class="form-control validate[required]" id="nombre_levanta" placeholder="Juan Pérez">
                    </div>
                    <div class="form-group">
                        <label for="direccion_levanta">
                            Dirección Física
                        </label>
                        <input type="text" class="form-control validate[required]" id="direccion_levanta" placeholder="10 calle 2-54 zona 1">
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label for="email_levanta">
                                Email
                            </label>
                            <input type="email" class="form-control validate[required,custom[email]]" id="email_levanta" placeholder="correo@correo.com">
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="telefono_levanta">
                                Teléfono contacto
                            </label>
                            <input type="text" class="form-control validate[required,custom[phone]]" id="telefono_levanta" placeholder="5555-5555">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label for="latitud_levanta">
                                Latitud
                            </label>
                            <input type="latitud" class="form-control validate[required]" id="latitud_levanta" placeholder="">
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="longitud_levanta">
                                Longitud
                            </label>
                            <input type="text" class="form-control validate[required]" id="longitud_levanta" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="telefono_levanta">
                            Comentarios
                        </label>
                        <textarea class="form-control" name="comentarios" id="comentarios" cols="30" rows="10"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary guardar_info">Guardar información</button>
            </div>
        </div>
    </div>
</div>
