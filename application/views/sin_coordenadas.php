<script>
$(document).ready(function(){
    $('#nodo').on('change',function(){
        $('#form_nodo').submit();
    });
});
</script>
<div class="container reporte">
    <h1>
        Direcciones sin coordenadas
        <div class="select_nodo">
            <div class="form-group">
                <form action="<?=site_url("update");?>" method="post" id="form_nodo">
                    <select name="nodo" id="nodo">
                        <option value="-1">
                            Seleccione un nodo
                        </option>
                        <?php foreach($nodos as $nodo):?>
                            <option <?php if($nodo->nodo==$nodo_seleccionado) echo "selected='selected'";?> value="<?=$nodo->nodo;?>">
                                <?=$nodo->nodo;?> (<?=$nodo->cant;?>)
                            </option>
                        <?php endforeach;?>
                    </select>
                </form>
            </div>
        </div>
    </h1>
    <h4>
        Con Coordenadas:
        <strong>
            <?=number_format($con);?>
        </strong>
        Sin Coordenadas:
        <strong>
            <?=number_format($sin);?>
        </strong>
        Completado:
        <strong>
            <?=number_format(($con/($sin+$con)*100),2);?>%
        </strong>
    </h4>
    <form action="<?=site_url('update/save');?>" method="post" id="form_nodos">
        <input name="nodo" id="nodo_submit" type="hidden" value="<?=$nodo_seleccionado;?>"/>
        <div class="primer_boton">
            <?php if(!empty($guardo)):?>
                <div class="alert alert-success alert-dismissible" role="alert" style="float: left; width: 400px;">
                    Se han guardado los datos con éxito
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                </div>
            <?php endif;?>
            <button type="submit" class="btn btn-primary pull-right">
                Guardar
            </button>
        </div>
        <table class="table table-bordered table-condensed pull-left">
            <tr>
                <th>
                    Nodo
                </th>
                <th>
                    Dirección
                </th>
                <th>
                    Lote
                </th>
                <th>
                    Manzana
                </th>
                <th>
                    Casa #
                </th>
                <th>
                    Autocad X
                </th>
                <th>
                    Autocad Y
                </th>
            </tr>
            <?php foreach($rows as $row):?>
                <tr>
                    <td>
                        <?=$row->NODO;?>
                    </td>
                    <td>
                        <?=$row->DIRECCION;?>
                    </td>
                    <td>
                        <?=$row->MANZANA;?>
                    </td>
                    <td>
                        <?=$row->LOTE;?>
                    </td>
                    <td>
                        <?=$row->CASA_No;?>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="id[]" name="id[]" value="<?=$row->id;?>">
                            <input type="text" class="form-control" id="latitud[]" name="latitud[]" placeholder="x">
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="text" class="form-control" id="longitud[]" name="longitud[]" placeholder="y">
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
        </table>
        <div class="ultimo_boton">
            <button type="submit" class="btn btn-primary pull-right">
                Guardar
            </button>
        </div>
    </form>
</div>