<div class="login_form">
    <div class="login_logo">
        <img src="<?=base_url();?>_img/logo.png" class="img-responsive">
    </div>
    <form method="post" action="<?=site_url("session/start");?>" class="form-horizontal" role="form" autocomplete="off">
        <fieldset>
            <div class="form-group">
                <label for="user_name" class="control-label">
                    Usuario
                </label>
                <input type="text" id="user_name" name="user_name" class="form-control" autocomplete="off" autofocus="autofocus">
            </div>
            <div class="form-group">
                <label for="password" class="control-label">
                    Contraseña
                </label>
                <input type="password" id="password" name="password" class="form-control" autocomplete="off">
            </div>
            <button type="submit">
                Ingresar
                <i class="icon-white icon-chevron-right"></i>
            </button>
            <?php if($this->uri->segment(2)==1):?>
                <div class="alert alert-danger pull-left">
                    Usuario y/o password incorrecto
                </div>
            <?php endif;?>
        </fieldset>
    </form>
</div>