<script>
$(document).ready(function(){
    $('#departamento,#municipio').on('change',function(){
        $('#descargar').val(0);
        $('#update').submit();
    });
    $('#descargar_archivo').on('click',function(){
        $('#descargar').val(1);
        $('#update').submit();
    });
});
</script>
<div class="container reporte">
    <h1>
        Nodos cargados <?=count($rows);?>
        <button class="btn btn-info btn-lg" id="descargar_archivo">
            <i class="glyphicon glyphicon-download-alt"></i>
            Descargar
        </button>
    </h1>
    <div class="row">
        <form id="update" action="<?=site_url("reporte/nodos_cargados");?>" method="post">
            <div class="col-lg-3 form-group">
                <select name="departamento" id="departamento" class="form-control">
                    <option value="">
                        Seleccione un Departamento
                    </option>
                    <?php foreach($deptos as $dep):?>
                        <option value="<?=$dep->departamento; ?>" <?php if($dep->departamento==$depto) echo "selected";?>>
                            <?=$dep->departamento;?>
                        </option>
                    <?php endforeach;?>
                </select>
                <input type="hidden" name="descargar" id="descargar" value="<?=$descargar;?>">
            </div>
            <div class="col-lg-3 form-group">
                <select name="municipio" id="municipio" class="form-control">
                    <option value="">
                        Seleccione un Municipio
                    </option>
                    <?php if(!empty($munis))
                    foreach($munis as $mun):?>
                        <option value="<?=$mun->municipio; ?>" <?php if($mun->municipio==$muni) echo "selected";?>>
                            <?=$mun->municipio;?>
                        </option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="col-lg-3 form-group">
                <select name="zona[]" id="zona" class="form-control" multiple>
                    <option value="">
                        Seleccione una zona
                    </option>
                    <?php if(!empty($zonas))
                        foreach($zonas as $zon):?>
                            <option value="<?=urlencode($zon->zona);?>" <?php if($zona && in_array($zon->zona,$zona)) echo "selected";?>>
                                <?=$zon->zona;?>
                            </option>
                        <?php endforeach;?>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">
                Update
            </button>
        </form>
    </div>
    <form action="<?=site_url('reporte/save');?>" method="post" id="form_nodos">
        <table class="table table-bordered table-condensed pull-left">
            <tr>
                <th>
                    Departamento
                </th>
                <th>
                    Municipio
                </th>
                <th>
                    Zona
                </th>
                <th>
                    Hub
                </th>
                <th>
                    Nodo
                </th>
                <th>
                    Cantidad de Direcciones
                </th>
                <!--
                    <th>
                        Direcciones con coordenadas
                    </th>
                    <th>
                        Direcciones sin coordenadas
                    </th>
                -->
            </tr>
            <?php foreach($rows as $row):?>
                <tr>
                    <td>
                        <?=$row['DEPARTAMENTO'];?>
                    </td>
                    <td>
                        <?=$row['MUNICIPIO'];?>
                    </td>
                    <td>
                        <?=$row['ZONA'];?>
                    </td>
                    <td>
                        <?=$row['hub'];?>
                    </td>
                    <td>
                        <?=$row['nodo'];?>
                    </td>
                    <td>
                        <?=number_format($row['cant']);?>
                    </td>
                    <!--
                    <td>
                        <?=number_format($row['coords']);?>
                    </td>
                    <td>
                        <?=number_format($row['sin_coords']);?>
                    </td>
                    -->
                </tr>
            <?php endforeach;?>
        </table>
    </form>
</div>