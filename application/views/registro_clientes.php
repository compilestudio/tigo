<script>
$(document).ready(function(){
    $('#nodo').on('change',function(){
        $('#form_nodo').submit();
    });
});
</script>
<div class="container reporte">
    <br>
    <h1>
        Reporte de clientes captados por todos los usuarios
        <a href="<?=site_url("reporte/descargar_clientes");?>" type="submit" class="btn btn-primary pull-right">
            Descargar
        </a>
    </h1>
    <br><br>
    <form action="<?=site_url('reporte/save');?>" method="post" id="form_nodos">
        <table class="table table-bordered table-condensed pull-left">
            <tr>
                <th>
                    Nombre
                </th>
                <th>
                    Dirección
                </th>
                <th>
                    Email
                </th>
                <th>
                    Teléfono
                </th>
                <th>
                    Longitud
                </th>
                <th>
                    Latitud
                </th>
                <th>
                    Comentarios
                </th>
                <th>
                    Acciones
                </th>
            </tr>
            <?php foreach($rows as $row):?>
                <tr>
                    <td id="nombre">
                        <?=$row->nombre;?>
                    </td>
                    <td id="direccion">
                        <?=$row->direccion;?>
                    </td>
                    <td id="email">
                        <?=$row->email;?>
                    </td>
                    <td id="telefono">
                        <?=$row->telefono;?>
                    </td>
                    <td id="longitud">
                        <?=$row->longitud;?>
                    </td>
                    <td id="latitud">
                        <?=$row->latitud;?>
                    </td>
                    <td id="comentarios">
                        <?=$row->comentarios;?>
                    </td>
                    <td data-id="<?=$row->id;?>">
                        <button type="button" class="btn btn-primary editar_usuario">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </button>
                        <button type="button" class="btn btn-danger delete_user">
                            <i class="glyphicon glyphicon-remove-sign"></i>
                        </button>
                    </td>
                </tr>
            <?php endforeach;?>
        </table>
    </form>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="editarUsuario">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">
            Editar Información
        </h4>
      </div>
      <div class="modal-body">
        <form id="edit_user">
            <div class="form-group">
                <label for="nombre">
                    Nombre
                </label>
                <input type="text" class="form-control validate[required]" id="nombre_" name="nombre">
            </div>
            <div class="form-group">
                <label for="direccion">
                    Dirección
                </label>
                <input type="text" class="form-control validate[required]" id="direccion_" name="direccion">
            </div>
            <div class="form-group">
                <label for="email">
                    Email
                </label>
                <input type="email" class="form-control validate[required]" id="email_" name="email">
            </div>
            <div class="form-group">
                <label for="telefono_">
                    Teléfono
                </label>
                <input type="text" class="form-control validate[required]" id="telefono_" name="telefono_">
            </div>
            <div class="form-group">
                <label for="longitud_">
                    Longitud
                </label>
                <input type="text" class="form-control validate[required]" id="longitud_" name="longitud_">
            </div>
            <div class="form-group">
                <label for="latitud_">
                    Latitud
                </label>
                <input type="text" class="form-control validate[required]" id="latitud_" name="latitud_">
            </div>
            <div class="form-group">
                <label for="comentarios_">
                    Comentarios
                </label>
                <textarea type="text" class="form-control validate[required]" id="comentarios_" name="comentarios_"></textarea>
                <input type="hidden" name="id" id="id_">
            </div>
            
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary guardar_edicion">Guardar Cambios</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
var row = null;
$(document).ready(function(){
    $("#edit_user").validationEngine('attach');
    /**
     * Editar Usuario
     */
    $('.table').on('click', '.editar_usuario' ,function(){
        $('#edit_user').validationEngine('hideAll');
        var obj = $(this).parent().parent();
        row = obj;
        $("#nombre_").val($.trim($("#nombre",$(obj)).text()));
        $("#direccion_").val($.trim($("#direccion",$(obj)).text()));
        $("#email_").val($.trim($("#email",$(obj)).text()));
        $("#telefono_").val($.trim($("#telefono",$(obj)).text()));
        $("#longitud_").val($.trim($("#longitud",$(obj)).text()));
        $("#latitud_").val($.trim($("#latitud",$(obj)).text()));
        $("#comentarios_").val($.trim($("#comentarios",$(obj)).text()));
        $("#id_").val($(this).parent().attr("data-id"));
        $('#editarUsuario').modal('show');
    });
    /**
     * Guardar edicion de registros
     */
    $('.guardar_edicion').on('click',function(){
        if($("#edit_user").validationEngine('validate')) {
            $.post(
                "<?=site_url('reporte/guardar_edits');?>",
                {
                    nombre: $('#nombre_').val(),
                    direccion: $('#direccion_').val(),
                    email: $('#email_').val(),
                    telefono: $('#telefono_').val(),
                    longitud: $('#longitud_').val(),
                    latitud: $('#latitud_').val(),
                    comentarios: $('#comentarios_').val(),
                    id: $('#id_').val()
                },function(data){
                    $("#nombre",$(row)).html($.trim($("#nombre_").val()));
                    $("#direccion",$(row)).html($.trim($("#direccion_").val()));
                    $("#email",$(row)).html($.trim($("#email_").val()));
                    $("#telefono",$(row)).html($.trim($("#telefono_").val()));
                    $("#longitud",$(row)).html($.trim($("#longitud_").val()));
                    $("#latitud",$(row)).html($.trim($("#latitud_").val()));
                    $("#comentarios",$(row)).html($.trim($("#comentarios_").val()));

                    $('#editarUsuario').modal('hide');
                    bootbox.alert("Usuario guardado con éxito!");
                }
            );
        }
    });
    /**
     * Eliminar Usuario
     */
    $('.table').on('click', '.delete_user', function(){
        var obj = $(this).parent().parent();
        var id = $(this).parent().attr("data-id");
        bootbox.confirm("¿Seguro que quiere eliminar este registro?", function(result) {
            if(result){
                $.post(
                    "<?=site_url('reporte/eliminar_registro');?>",
                    {
                        id:id
                    },
                    function(data){
                        $(obj).fadeOut();
                        bootbox.alert("Registro eliminado!");
                    }
                );
            }
        });
    });
});
</script>