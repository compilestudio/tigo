<div class="container reporte">
    <h1>
        Listado de colonias/condominios/residenciales
        <small>
            <select id="op">
                <option value="0" <?php if($op==0) echo "selected";?>>Nuevas</option>
                <option value="1" <?php if($op==1) echo "selected";?>>Adquiridas</option>
            </select>
        </small>
    </h1>
    <?php if(!empty($rows)):?>
        <ul class="colonias_list">
            <?php foreach($rows as $row):?>
                <li>
                    <?=ucwords(strtolower($row->colonias));?>
                </li>
            <?php endforeach?>
        </ul>
    <?php endif;?>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#op').on('change',function(){
        location.href = '<?=site_url("herramienta/listado_colonias/");?>/'+$(this).val();
    });
});
</script>