<?php

	/*
	 * Fecha de Creación: 18-nov-2011
	 * Autor: Víctor López
	 * Fecha Última Modificación: 18-nov-2011
	 * Modificado por: Víctor López
	 * Descripción: Modelo con funciones genéricas para acceso a base de datos
	 * 
	 */ 

	class MY_Model extends CI_Model{
			
		// Contiene el nombre de la tabla que representa el modelo
		protected $_table;
		protected $idioma;
		
	public function __construct(){
		parent::__construct();
		//Establecemos el idioma ingles como el basico o el que se haya seleccionado
		$this->idioma = "english";
		if($this->input->cookie('idioma', TRUE))
			$this-> idioma =$this->input->cookie('idioma', TRUE);
	}
		
	// obtenemos el idioma establecido
	public function get_idioma() {
		return $this->idioma;
	}
				
		// Establece el nombre de la tabla
		public function set_table($table_name) {
			$this->_table = $table_name;
		}
		
		/* Inserta un nuevo registro en la tabla
		 * $data - arreglo asociativo con las columnas => valores a insertar
		 */
		public function insert($data) {
										
			$this->db->insert($this->_table, $data);
			return $this->db->insert_id();
		}
		
		/* Actualiza un registro de la tabla
		 * $data - arrego asosciativo con columnas => nuevos valores
		 * $id - id del registro que se quiere actualizar
		 */
		public function update($data, $id) {
			
			$this->db->where('id', $id);			
			$this->db->update($this->_table, $data);
			
		}
		
		/* Elimina un registro de la tabla
		 * $id - id del registro que se quiere eliminar
		 */
		public function delete($id) {
			
			$this->db->where('id', $id);
			$this->db->delete($this->_table);
			
		}
		
		/*
		 * Devuelve el total de columnas de la tabla, opcionalmente, se puede enviar
		 * un string con el 'where' para contar solo las filas que coinciden
		 */
		public function total_rows($where = '') {
			
			if ($where != '') :
				
				$this->db->select('COUNT(1) AS total', false);
				$this->db->from($this->_table);
				$this->db->where($where);
				
				$query = $this->db->get();
				$row = $query->row();
				return $row->total; 

				
			else :
				
				return $this->db->count_all($this->_table);
				
			endif;
			
		}
		
		/*
		 * Devuelve un conjunto de filas de la tabla
		 * $columns - string con los nombres de las columnas que se quieren obtener, separadas por comas
		 * $limit - cantidad de rows que se quieren obtener
		 * $offset - # de fila inicial que se quiere obtener
		 * $where - condiciones para aplicar a la consulta
		 * 
		 */
		public function list_rows($columns = '', $where = '',$order_by= '', $limit = NULL, $offset = NULL) {
			
			if ($columns != '') :
				$this->db->select($columns);
			endif;
			
			$this->db->from($this->_table);
			
			if ($where != '') :
				$this->db->where($where, NULL, FALSE);
			endif;
			
			if ($order_by != '') :
				$this->db->order_by($order_by); 
			endif;
			
			if ($limit != NULL) :
				
				if ($offset != NULL) :
				
					$this->db->limit($limit, $offset);
					
				else :
					
					$this->db->limit($limit);
				endif; 
				
			endif;
			
			$query = $this->db->get();
			
			if ($query->num_rows() > 0):
				return $query->result();
			else:
				return 0;
			endif;
		}

		/*
		 * Retorna una fila de la tabla en un objeto cuyas propiedades son las columnas
		 * 
		 * $id - el id en la tabla
		 * $format - el formato en el que se quiere obtener el objecto:
		 * 			- 'object' - retorna el objeto que devuelve CI->db->get()->row()
		 *      - 'json'   - retorna el mismo objeto codificado en JSON con json_encode
		 *      - 'xml'    - retorna el mismo objecto codificado como XML (el elemento raíz se llama 'root')
		 * 
		 * Si el ID que se envió no existe en la tabla, retorna FALSE
		 * 
		 */
		public function get($id, $format = 'object') {
			
			$this->db->where('id', $id);						
			$query = $this->db->get($this->_table);
			
			#verificamos si existe el id
			if ($query->num_rows > 0)  {
				
				$object =  $query->row();
				
				switch ($format) {
					
					case 'json' :
						return json_encode($object);
						break;
					case 'xml' :
						$xml =  $this->_xml_encode($object);
						return $xml;
						break;
					default:
						return $object;
						break;
				}
				
			}
			
			#si no existe, devolvemos FALSE
			else {
				return FALSE;
			}
		
		}
		
		
		protected function _xml_encode($object) {
				
			$array = array();
			
			#creamos la raíz del xml	
			$xml = new SimpleXMLElement('<root/>');
			
			#convertimos el objeto a un arreglo
			foreach($object as $key => $value) {
				$xml->addChild($key, $value);	
			}
			
			#devolvemos el XML como string
			return utf8_encode(html_entity_decode($xml->asXML()));

		} 
		
	}

?>