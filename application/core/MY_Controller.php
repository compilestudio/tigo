<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Fecha Creación: 21 de noviembre del 2011
 * Autor: Pedro Izaguirre
 * Fecha última modificación: 21 de noviembre del 2011
 * Último en modificarla: Pedro Izaguirre
 *
 * Descripción: Son las funciones que tendrán todos los controladores
 * */


class MY_Controller extends CI_Controller {

	protected $idioma;

	public function __construct(){
		parent::__construct();

		$ip = $this->get_client_ip();
		$this->load->model("file_model","file");
		$this->file->set_table("tr_cldr");
		$ip_existe = $this->file->list_rows('*',"mjm='".$ip."'");
		#var_dump($ip_existe);
		#echo $ip;

		if(empty($ip_existe)):
			$message_403 = "Usted no tiene acceso a esta p&aacute;gina".$ip;
			show_error($message_403 , 403 );
		endif;

		#Si existe el usuario
		$this->load->model("session_model");
		$user = $this->session->userdata('user_tigo');
		if(!empty($user)):
			$sql = "SELECT TIMESTAMPDIFF(MINUTE,logout,NOW()) minutes
					FROM `bitacora`
					where   user=".$user."
					order by login desc";
			/*
			$sql = "select  *
		        from    bitacora
				where   user=".$user."
				and     logout='0000-00-00 00:00:00'";
			*/
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0):
				$res = $query->result();
				if ($res[0]->minutes>10):
					$this->session_model->save_login($user);
				endif;
			else:
				$this->session_model->save_login($user);
			endif;
		endif;

		#Redirect on backend
		if ($this->uri->segment(1)!= "session" && $this->uri->segment(1)!= "landing" && $this->uri->segment(1)!= "kml" && $this->uri->segment(2)!= "upload_file" && $this->uri->segment(2)!= "upload_file_kml" && $this->uri->segment(2)!= "upload_file_kml_adquirida" && $this->uri->segment(2)!= "upload_file_colonias" && $this->session->userdata('user_tigo') === FALSE)
			redirect("landing");

		$tipo = $this->session->userdata('tigo_tipo');
		$c =$this->router->class;
		if($tipo==2 && !($c=="mapa" || $c=="reporte" || $c=="update" || $c=="session" || $c=="polygon")):
			redirect("mapa");
		endif;
		if($tipo==3 && !($c=="mapa" || $c=="session" || $c=="polygon")):
			redirect("mapa");
		endif;
	}
#****************************************************************************************************************
	#Funcion para cargar el header
	protected function load_header($title){

		$user_id = $this->session->userdata('user_tigo');
		$dataHeader["page_title"]=$title;
		#$dataHeader['main_menu_title'] = $this->lang->line('main_menu_title');

        $dataHeader['forced'] = $this->session->userdata('tigo_force');

		$this->load->view('_includes/header', $dataHeader);
	}
#****************************************************************************************************************
	#Funcion para cargar el footer
	protected function load_footer(){
		$this->load->view('_includes/footer');
	}
#**************************************************************************************************
	// Function to get the client IP address
	public function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
}
