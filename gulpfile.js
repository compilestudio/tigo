var gulp = require('gulp');
var watchLess = require('gulp-watch-less');
var less = require('gulp-less');

gulp.task('default', function () {
    return gulp.src('_css/*.less')
        .pipe(watchLess('_css/*.less'))
        .pipe(debug())
        .pipe(less({compress: true}))
        .pipe(gulp.dest('_css'));
});