<?php

	$this->set_css($this->default_theme_path.'/JCrud/css/flexigrid.css');
	$this->set_js_lib($this->default_theme_path.'/flexigrid/js/jquery.form.js');
	$this->set_js_config($this->default_theme_path.'/flexigrid/js/flexigrid-edit.js');

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>



<div class="row" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">

	<div id='main-table-box'>
		<h2><a href=""><span class="glyphicon glyphicon-pencil"></span> <?php echo $this->l('form_edit'); ?> <?php echo $subject?></a></h2>

		<?php echo form_open( $update_url, 'method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
		<div class='row'> <!-- begin col-md-12 -->
			<?php
			$counter = 0;
				foreach($fields as $field)
				{
					$even_odd = $counter % 2 == 0 ? 'odd' : 'even';
					$counter++;
			?>
			<div class="col-md-6 form-group">
				<div class=' <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box">
					<div id="<?php echo $field->field_name; ?>_display_as_box">
						<label class="col-sm-2 control-label"><?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?></label>	 
					</div>
					<div  id="<?php echo $field->field_name; ?>_input_box">
						<?php echo $input_fields[$field->field_name]->input?>
					</div>
					<div class='clear'></div>
				</div>
			</div>	
			<?php }?>
			<?php if(!empty($hidden_fields)){?>
			<!-- Start of hidden inputs -->
				<?php
					foreach($hidden_fields as $hidden_field){
						echo $hidden_field->input;
					}
				?>
			<!-- End of hidden inputs -->
			<?php }?>
			<?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php }?>
			<div id='report-error' class='report-div error'></div>
			<div id='report-success' class='report-div success'></div>
		</div><!-- end col-md-12 -->

		<div class="row"><!-- begin col-md-12 -->
			<div class="col-md-3">
			</div>	
			<div class="col-md-9 col-md-offset-3">
				<input  id="form-button-save" type='submit' value='<?php echo $this->l('form_update_changes'); ?>' class="btn btn-large btn-success"/>			
				<?php 	if(!$this->unset_back_to_list) { ?>			
				<input type='button' value='<?php echo $this->l('form_update_and_go_back'); ?>' id="save-and-go-back-button" class="btn btn-large btn-success"/>						
				<input type='button' value='<?php echo $this->l('form_cancel'); ?>' class="btn btn-large btn-success" id="cancel-button" />			
				<?php 	} ?>				
			</div>
			
		</div><!-- end col-md-12 -->

		<?php echo form_close(); ?>
	</div>
</div>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';

	var message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>";
	var message_update_error = "<?php echo $this->l('update_error')?>";
</script>