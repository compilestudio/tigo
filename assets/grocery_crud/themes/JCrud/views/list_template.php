<?php
	$this->set_css($this->default_theme_path.'/JCrud/css/flexigrid.css');
	$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
	$this->set_js_lib($this->default_javascript_path.'/common/lazyload-min.js');

	if (!$this->is_IE7()) {
		$this->set_js_lib($this->default_javascript_path.'/common/list.js');
	}

	$this->set_js($this->default_theme_path.'/flexigrid/js/cookies.js');
	$this->set_js($this->default_theme_path.'/flexigrid/js/flexigrid.js');
	$this->set_js($this->default_theme_path.'/flexigrid/js/jquery.form.js');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.numeric.min.js');
	$this->set_js($this->default_theme_path.'/flexigrid/js/jquery.printElement.min.js');

	/** Fancybox */
	$this->set_css($this->default_css_path.'/jquery_plugins/fancybox/jquery.fancybox.css');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.fancybox-1.3.4.js');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.easing-1.3.pack.js');

	/** Jquery UI */
	$this->load_js_jqueryui();

?>
<script type='text/javascript'>
	var base_url = '<?php echo base_url();?>';

	var subject = '<?php echo $subject?>';
	var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
	var unique_hash = '<?php echo $unique_hash; ?>';

	var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";

</script>

<div id='list-report-error' class='report-div error'></div>
<div id='list-report-success' class='report-div success report-list' <?php if($success_message !== null){?>style="display:block"<?php }?>><?php
if($success_message !== null){?>
	<p><?php echo $success_message; ?></p>
<?php }
?></div>
<div class="flexigrid" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
	<div id="hidden-operations" class="hidden-operations"></div>
	<div class="mDiv">
		<div class="ftitle">
			&nbsp;
		</div>
		<div title="<?php echo $this->l('minimize_maximize');?>" class="ptogtitle">
			<span></span>
		</div>
	</div>



<div id='main-table-box'><!--begin contenedor-->

	<?php if(!$unset_add || !$unset_export || !$unset_print){?>	
	<div class="row"><!-- begin div contenedor opciones CRUD-->
		<div class="col-md-12">
			<?php if(!$unset_add){?>		
			<div class="col-md-4">
	        	<a href='<?php echo $add_url?>' title='<?php echo $this->l('list_add'); ?> <?php echo $subject?>' class='add-anchor add_button'>			
					<h2><span class="glyphicon glyphicon-plus-sign"></span> <?php echo $this->l('list_add'); ?> <?php echo $subject?></h2>			
	            </a>			
			</div>
			<?php }?>



			<div class="col-md-4">
				<?php if(!$unset_export) { ?>
	        	<a class="export-anchor" data-url="<?php echo $export_url; ?>" target="_blank">
					<h2><span class="glyphicon glyphicon-file"></span><?php echo $this->l('list_export');?></h2>
	            </a>			
				<?php } ?>
			</div>	
			
			<?php if(!$unset_print) { ?>
	        <div class="col-md-4">	
	        	<a class="print-anchor" data-url="<?php echo $print_url; ?>">				
					<h2><span class="glyphicon glyphicon-print"></span> <?php echo $this->l('list_print');?></h2>
	            </a>
	         
				 
			  </div>
	        </div>    
			<?php }?>			
		</div>
	</div><!--end div contenedor opciones CRUD-->	
	<?php }?>

	<div class="row">
		 <div class="pReload  ajax_refresh_and_loading" id='ajax_refresh_and_loading'>
	</div>
	
	<div id='ajax_list' class="ajax_list">
		<?php echo $list_view?>
	</div>

<?php echo form_open( $ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="'.$ajax_list_info_url.'"'); ?>	
	<div class="row"><!--begin div contenedor form-->
		<div class="col-mod-12" id='quickSearchBox'><!--begin div  col-md-12 -->		
			<div class="col-xs-2">
				<select name="per_page" id='per_page' class="per_page form-control">
					<?php foreach($paging_options as $option){?>
						<option value="<?php echo $option; ?>" <?php if($option == $default_per_page){?>selected="selected"<?php }?>><?php echo $option; ?>&nbsp;&nbsp;</option>
					<?php }?>
				</select>		
			</div>
			<div class="col-xs-3">				
				<select name="search_field" id="search_field" class="form-control">
					<option value=""><?php echo $this->l('list_search_all');?></option>
						<?php foreach($columns as $column){?>
					<option value="<?php echo $column->field_name?>"><?php echo $column->display_as?>&nbsp;&nbsp;</option>
					<?php }?>
				</select>   			
			</div>	
			<div class="col-xs-5">				
    			<div class="input-group"><!-- begin input-group --> 
					<span class="input-group-addon">Qué Desea Buscar</span>
					<input type="text" class="form-control qsbsearch_fieldox search_text" name="search_text" size="30" id='search_text'>
 					<span class="input-group-btn">
		   				 <button type="submit" id='crud_search' class="btn btn-success"><span class="glyphicon glyphicon-search"></span> <?php echo $this->l('list_search');?></button>	    		
 					</span>
    			</div><!-- end input-group -->     			
			</div>
			<div class="col-md-1">
				<input type="reset" class="search_clear btn btn-success" value="<?php echo $this->l('list_clear_filtering');?>">
			</div>    	
		</div><!--end div col-md-12 -->  
	</div><!--end div contenedor form-->
	

	
		<div class="col-md-12"><!-- begin col-md-12 -->			
			<ul class="pager">				
			  	<li class="previous pFirst pButton first-button">
			  		<a href="#">&larr;Primero</a>
			  	</li>
			  	<li class="prev-button">
			  		<a>Anterior</a>
			  	</li>
			  	
			  	<li>
			  		<a href="#">
			  			<span class="pcontrol">
			  			<?php echo $this->l('list_page'); ?>
			  			<input name='page' type="text" value="1" size="4" id='crud_page' class="crud_page input-pagination">
			  			<?php echo $this->l('list_paging_of'); ?>
			  			<span id='last-page-number' class="last-page-number"><?php echo ceil($total_results / $default_per_page)?></span></span>
			  			<span class="pPageStat">
							<?php $paging_starts_from = "<span id='page-starts-from' class='page-starts-from'>1</span>"; ?>
							<?php $paging_ends_to = "<span id='page-ends-to' class='page-ends-to'>". ($total_results < $default_per_page ? $total_results : $default_per_page) ."</span>"; ?>
							<?php $paging_total_results = "<span id='total_items' class='total_items'>$total_results</span>"?>
							<?php echo str_replace( array('{start}','{end}','{results}'),
													array($paging_starts_from, $paging_ends_to, $paging_total_results),
													$this->l('list_displaying')
												   ); ?>
						</span>
			  		</a>
			  	</li>

			  	<li class=" next-button">
			  		<a href="#">Siguiente</a>
			  	</li>
			  	<li class="next pLast pButton last-button">
			  		<a href="#">Ultimo &rarr;</a>
			  	</li>
			</ul>
			<!-- Input tipo hidden tienen esta opción debido que hay mostramos el orden que la persona desea mostar sus registros -->
			<input type='hidden' name='order_by[0]' id='hidden-sorting' class='hidden-sorting' value='<?php if(!empty($order_by[0])){?><?php echo $order_by[0]?><?php }?>' />
			<input type='hidden' name='order_by[1]' id='hidden-ordering' class='hidden-ordering'  value='<?php if(!empty($order_by[1])){?><?php echo $order_by[1]?><?php }?>'/>
									
		</div><!-- end col-col-12 -->
	
<?php echo form_close(); ?>	
</div><!--end div contenedor-->
